(function ($) {
	"use strict";
	
	var subscribe = {};
	qodef.modules.subscribe = subscribe;
	
	subscribe.qodefOnDocumentReady = qodefOnDocumentReady;
	
	$(document).ready(qodefOnDocumentReady);
	
	function qodefOnDocumentReady() {
		qodefInitTeamMemberPopup();
	}
	
	function qodefInitTeamMemberPopup() {
		var popupOpener = $('.qodef-ts-contact-button a'),
			popupClose = $('.qodef-tmp-close');
		
		popupOpener.on('click', function (e) {
			e.preventDefault();
			
			if (qodef.body.hasClass('qodef-tm-popup-opened')) {
				qodef.body.removeClass('qodef-tm-popup-opened');
				qodef.modules.common.qodefEnableScroll();
			} else {
				qodef.body.addClass('qodef-tm-popup-opened');
				qodef.modules.common.qodefDisableScroll();
			}
			
			popupClose.on('click', function (e) {
				e.preventDefault();
				
				qodef.body.removeClass('qodef-tm-popup-opened');
				qodef.modules.common.qodefEnableScroll();
			});
			
			//Close on escape
			$(document).keyup(function (e) {
				if (e.keyCode === 27) { //KeyCode for ESC button is 27
					qodef.body.removeClass('qodef-tm-popup-opened');
					qodef.modules.common.qodefEnableScroll();
				}
			});
		});
	}
	
})(jQuery);