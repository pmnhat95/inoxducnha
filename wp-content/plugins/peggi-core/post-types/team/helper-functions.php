<?php

if ( ! function_exists( 'peggi_core_team_meta_box_functions' ) ) {
	function peggi_core_team_meta_box_functions( $post_types ) {
		$post_types[] = 'team-member';
		
		return $post_types;
	}
	
	add_filter( 'peggi_select_filter_meta_box_post_types_save', 'peggi_core_team_meta_box_functions' );
	add_filter( 'peggi_select_filter_meta_box_post_types_remove', 'peggi_core_team_meta_box_functions' );
}

if ( ! function_exists( 'peggi_core_team_scope_meta_box_functions' ) ) {
	function peggi_core_team_scope_meta_box_functions( $post_types ) {
		$post_types[] = 'team-member';
		
		return $post_types;
	}
	
	add_filter( 'peggi_select_filter_set_scope_for_meta_boxes', 'peggi_core_team_scope_meta_box_functions' );
}

if ( ! function_exists( 'peggi_core_team_enqueue_meta_box_styles' ) ) {
	function peggi_core_team_enqueue_meta_box_styles() {
		global $post;
		
		if ( $post->post_type == 'team-member' ) {
			wp_enqueue_style( 'qodef-jquery-ui', get_template_directory_uri() . '/framework/admin/assets/css/jquery-ui/jquery-ui.css' );
		}
	}
	
	add_action( 'peggi_select_action_enqueue_meta_box_styles', 'peggi_core_team_enqueue_meta_box_styles' );
}

if ( ! function_exists( 'peggi_core_register_team_cpt' ) ) {
	function peggi_core_register_team_cpt( $cpt_class_name ) {
		$cpt_class = array(
			'PeggiCore\CPT\Team\TeamRegister'
		);
		
		$cpt_class_name = array_merge( $cpt_class_name, $cpt_class );
		
		return $cpt_class_name;
	}
	
	add_filter( 'peggi_core_filter_register_custom_post_types', 'peggi_core_register_team_cpt' );
}

// Load team shortcodes
if(!function_exists('peggi_core_include_team_shortcodes_files')) {
    /**
     * Loades all shortcodes by going through all folders that are placed directly in shortcodes folder
     */
    function peggi_core_include_team_shortcodes_files() {
        foreach(glob(PEGGI_CORE_CPT_PATH.'/team/shortcodes/*/load.php') as $shortcode_load) {
            include_once $shortcode_load;
        }
    }

    add_action('peggi_core_action_include_shortcodes_file', 'peggi_core_include_team_shortcodes_files');
}

if ( ! function_exists( 'peggi_core_get_single_team' ) ) {
	/**
	 * Loads holder template for doctor single
	 */
	function peggi_core_get_single_team() {
		$team_member_id = get_the_ID();
		
		$params = array(
			'sidebar_layout' => peggi_select_sidebar_layout(),
			'position'       => get_post_meta( $team_member_id, 'qodef_team_member_position', true ),
			'birth_date'     => get_post_meta( $team_member_id, 'qodef_team_member_birth_date', true ),
			'email'          => get_post_meta( $team_member_id, 'qodef_team_member_email', true ),
			'phone'          => get_post_meta( $team_member_id, 'qodef_team_member_phone', true ),
			'address'        => get_post_meta( $team_member_id, 'qodef_team_member_address', true ),
			'education'      => get_post_meta( $team_member_id, 'qodef_team_member_education', true ),
			'resume'         => get_post_meta( $team_member_id, 'qodef_team_member_resume', true ),
			'hover_image'    => get_post_meta( $team_member_id, 'qodef_team_member_hover_image', true ),
			'social_icons'   => peggi_core_single_team_social_icons( $team_member_id ),
			'selected_events'     => get_post_meta( $team_member_id, 'qodef_team_member_event_ids', true )
		);
		
		peggi_core_get_cpt_single_module_template_part( 'templates/single/holder', 'team', '', $params );
	}
}


if ( ! function_exists( 'peggi_core_single_team_social_icons' ) ) {
	function peggi_core_single_team_social_icons( $id ) {
		$social_icons = array();
		
		for ( $i = 1; $i < 6; $i ++ ) {
			$team_icon_pack = get_post_meta( $id, 'qodef_team_member_social_icon_pack_' . $i, true );
			if ( $team_icon_pack !== '' ) {
				$team_icon_collection = peggi_select_icon_collections()->getIconCollection( get_post_meta( $id, 'qodef_team_member_social_icon_pack_' . $i, true ) );
				$team_social_icon     = get_post_meta( $id, 'qodef_team_member_social_icon_pack_' . $i . '_' . $team_icon_collection->param, true );
				$team_social_link     = get_post_meta( $id, 'qodef_team_member_social_icon_' . $i . '_link', true );
				$team_social_target   = get_post_meta( $id, 'qodef_team_member_social_icon_' . $i . '_target', true );
				
				if ( $team_social_icon !== '' ) {
					$team_icon_params                                 = array();
					$team_icon_params['icon_pack']                    = $team_icon_pack;
					$team_icon_params[ $team_icon_collection->param ] = $team_social_icon;
					$team_icon_params['link']                         = ! empty( $team_social_link ) ? $team_social_link : '';
					$team_icon_params['target']                       = ! empty( $team_social_target ) ? $team_social_target : '_self';
					
					$social_icons[] = peggi_select_execute_shortcode( 'qodef_icon', $team_icon_params );
				}
			}
		}
		
		return $social_icons;
	}
}

if ( ! function_exists( 'peggi_core_get_team_category_list' ) ) {
	function peggi_core_get_team_category_list( $category = '' ) {
		$number_of_columns = 3;
		
		$params = array(
			'number_of_columns' => $number_of_columns
		);
		
		if ( ! empty( $category ) ) {
			$params['category'] = $category;
		}
		
		$html = peggi_select_execute_shortcode( 'qodef_team_list', $params );
		
		print $html;
	}
}

if ( ! function_exists( 'peggi_core_get_team_member_event_list' ) ) {
	function peggi_core_get_team_member_event_list( $selected_events = '' ) {
		$number_of_columns = 'three';
		$type = 'simple';
		
		$params = array(
			'number_of_columns' => $number_of_columns,
			'type' => $type
		);
		
		if ( ! empty( $selected_events ) ) {
			$params['selected_events'] = $selected_events;
		}
		
		$html = peggi_select_execute_shortcode( 'qodef_events_list', $params );
		
		print $html;
	}
}

if ( ! function_exists( 'peggi_core_add_team_to_search_types' ) ) {
	function peggi_core_add_team_to_search_types( $post_types ) {
		$post_types['team-member'] = esc_html__( 'Team Member', 'peggi-core' );
		
		return $post_types;
	}
	
	add_filter( 'peggi_select_filter_search_post_type_widget_params_post_type', 'peggi_core_add_team_to_search_types' );
}

if ( ! function_exists( 'qodef_themename_load_team_member_popup_template' ) ) {
	/**
	 * Loads HTML template with parameters
	 */
	function qodef_themename_load_team_member_popup_template() {
		$team_member_id           = get_the_ID();
		$team_member_contact_form = get_post_meta( $team_member_id, 'qodef_team_member_popup_contact_form_meta', true );
		
		if ( ! empty( $team_member_contact_form ) ) {
			$params = array();
			
			$params['contact_form']       = $team_member_contact_form;
			$params['contact_form_style'] = get_post_meta( $team_member_id, 'qodef_team_member_popup_contact_form_style_meta', true );
			$params['title']              = get_post_meta( $team_member_id, 'qodef_team_member_popup_cf_title_meta', true );
			$params['separator']          = get_post_meta( $team_member_id, 'qodef_team_member_popup_cf_enable_separator_meta', true );
			
			$content_styles = array();
			$bg_image       = get_post_meta( $team_member_id, 'qodef_team_member_popup_background_image_meta', true );
			
			if ( ! empty( $bg_image ) ) {
				$content_styles[] = 'background-image: url(' . esc_url( $bg_image ) . ')';
			}
			
			$params['content_styles'] = $content_styles;
			
			peggi_core_get_cpt_single_module_template_part('templates/single/parts/popup', 'team', '', $params);
		}
	}
	
	add_action( 'peggi_select_action_before_page_header', 'qodef_themename_load_team_member_popup_template' );
}

if ( ! function_exists( 'qodef_themename_load_team_member_popup_template' ) ) {
	/**
	 * Loads HTML template with parameters
	 */
	function qodef_themename_load_team_member_popup_template() {
		$team_member_id           = get_the_ID();
		$team_member_contact_form = get_post_meta( $team_member_id, 'qodef_team_member_popup_contact_form_meta', true );
		
		if ( ! empty( $team_member_contact_form ) ) {
			$params = array();
			
			$params['contact_form']       = $team_member_contact_form;
			$params['contact_form_style'] = get_post_meta( $team_member_id, 'qodef_team_member_popup_contact_form_style_meta', true );
			$params['title']              = get_post_meta( $team_member_id, 'qodef_team_member_popup_cf_title_meta', true );
			$params['separator']          = get_post_meta( $team_member_id, 'qodef_team_member_popup_cf_enable_separator_meta', true );
			
			$content_styles = array();
			$bg_image       = get_post_meta( $team_member_id, 'qodef_team_member_popup_background_image_meta', true );
			
			if ( ! empty( $bg_image ) ) {
				$content_styles[] = 'background-image: url(' . esc_url( $bg_image ) . ')';
			}
			
			$params['content_styles'] = $content_styles;
			
			peggi_core_get_cpt_single_module_template_part('templates/single/parts/popup', 'team', '', $params);
		}
	}
	
	add_action( 'peggi_select_action_before_page_header', 'qodef_themename_load_team_member_popup_template' );
}