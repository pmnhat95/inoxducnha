<?php

if ( ! function_exists( 'peggi_core_map_team_single_meta' ) ) {
	function peggi_core_map_team_single_meta() {
		$cf7 = get_posts( 'post_type="wpcf7_contact_form"&numberposts=-1' );
		
		$contact_forms = array();
		if ( $cf7 ) {
			$contact_forms[''] = esc_html__( 'Default', 'peggi-core' );
			
			foreach ( $cf7 as $cform ) {
				$contact_forms[ $cform->ID ] = $cform->post_title;
			}
		} else {
			$contact_forms[0] = esc_html__( 'No contact forms found', 'peggi-core' );
		}
		
		$meta_box = peggi_select_create_meta_box(
			array(
				'scope' => 'team-member',
				'title' => esc_html__( 'Team Member Info', 'peggi-core' ),
				'name'  => 'team_meta'
			)
		);
		
		peggi_select_create_meta_box_field(
			array(
				'name'        => 'qodef_team_member_position',
				'type'        => 'text',
				'label'       => esc_html__( 'Position', 'peggi-core' ),
				'description' => esc_html__( 'The members\'s role within the team', 'peggi-core' ),
				'parent'      => $meta_box
			)
		);
		
		peggi_select_create_meta_box_field(
			array(
				'name'        => 'qodef_team_member_birth_date',
				'type'        => 'date',
				'label'       => esc_html__( 'Birth date', 'peggi-core' ),
				'description' => esc_html__( 'The members\'s birth date', 'peggi-core' ),
				'parent'      => $meta_box
			)
		);
		
		peggi_select_create_meta_box_field(
			array(
				'name'        => 'qodef_team_member_email',
				'type'        => 'text',
				'label'       => esc_html__( 'Email', 'peggi-core' ),
				'description' => esc_html__( 'The members\'s email', 'peggi-core' ),
				'parent'      => $meta_box
			)
		);
		
		peggi_select_create_meta_box_field(
			array(
				'name'        => 'qodef_team_member_phone',
				'type'        => 'text',
				'label'       => esc_html__( 'Phone', 'peggi-core' ),
				'description' => esc_html__( 'The members\'s phone', 'peggi-core' ),
				'parent'      => $meta_box
			)
		);
		
		peggi_select_create_meta_box_field(
			array(
				'name'        => 'qodef_team_member_address',
				'type'        => 'text',
				'label'       => esc_html__( 'Address', 'peggi-core' ),
				'description' => esc_html__( 'The members\'s addres', 'peggi-core' ),
				'parent'      => $meta_box
			)
		);
		
		peggi_select_create_meta_box_field(
			array(
				'name'        => 'qodef_team_member_education',
				'type'        => 'text',
				'label'       => esc_html__( 'Education', 'peggi-core' ),
				'description' => esc_html__( 'The members\'s education', 'peggi-core' ),
				'parent'      => $meta_box
			)
		);
		
		peggi_select_create_meta_box_field(
			array(
				'name'        => 'qodef_team_member_resume',
				'type'        => 'file',
				'label'       => esc_html__( 'Resume', 'peggi-core' ),
				'description' => esc_html__( 'Upload members\'s resume', 'peggi-core' ),
				'parent'      => $meta_box
			)
		);
		
		peggi_select_create_meta_box_field(
			array(
				'name'        => 'qodef_team_member_hover_image',
				'type'        => 'image',
				'label'       => esc_html__( 'Hover Image', 'peggi-core' ),
				'description' => esc_html__( 'Upload members\'s hover image', 'peggi-core' ),
				'parent'      => $meta_box
			)
		);
		
		peggi_select_create_meta_box_field(
			array(
				'name'        => 'qodef_team_member_popup_cf_title_meta',
				'type'        => 'text',
				'label'       => esc_html__( 'Title', 'peggi-core' ),
				'description' => esc_html__( 'Popup Title', 'peggi-core' ),
				'parent'      => $meta_box
			)
		);
		
		peggi_select_create_meta_box_field(
			array(
				'name'          => 'qodef_team_member_popup_cf_enable_separator_meta',
				'type'          => 'yesno',
				'default_value' => 'yes',
				'label'         => esc_html__( 'Enable Separator', 'peggi-core' ),
				'description'   => esc_html__( 'Enabling this option will put separator below title', 'peggi-core' ),
				'parent'        => $meta_box,
			)
		);
		
		peggi_select_create_meta_box_field(
			array(
				'name'          => 'qodef_team_member_popup_contact_form_meta',
				'type'          => 'select',
				'default_value' => '',
				'label'         => esc_html__( 'Select Contact Form', 'peggi-core' ),
				'description'   => esc_html__( 'Choose contact form to display in popup', 'peggi-core' ),
				'parent'        => $meta_box,
				'options'       => $contact_forms
			)
		);
		
		peggi_select_create_meta_box_field(
			array(
				'name'          => 'qodef_team_member_popup_contact_form_style_meta',
				'type'          => 'select',
				'default_value' => '',
				'label'         => esc_html__( 'Contact Form Style', 'peggi-core' ),
				'description'   => esc_html__( 'Choose style defined in Contact Form 7 option tab', 'peggi-core' ),
				'parent'        => $meta_box,
				'options'       => array(
					'default'            => esc_html__( 'Default', 'peggi-core' ),
					'cf7_custom_style_1' => esc_html__( 'Custom Style 1', 'peggi-core' ),
					'cf7_custom_style_2' => esc_html__( 'Custom Style 2', 'peggi-core' ),
					'cf7_custom_style_3' => esc_html__( 'Custom Style 3', 'peggi-core' )
				)
			)
		);
		
		peggi_select_create_meta_box_field(
			array(
				'name'   => 'qodef_team_member_popup_background_image_meta',
				'type'   => 'image',
				'label'  => esc_html__( 'Contact Form Background Image', 'peggi-core' ),
				'parent' => $meta_box
			)
		);
		
		peggi_select_create_meta_box_field(
			array(
				'name'   => 'qodef_team_member_event_ids',
				'type'   => 'text',
				'label'  => esc_html__( 'Show Only Events with Listed IDs', 'peggi-core' ),
				'description' => esc_html__( 'Delimit ID numbers by comma (leave empty for all)', 'peggi-core' ),
				'parent' => $meta_box
			)
		);
		
		for ( $x = 1; $x < 6; $x ++ ) {
			
			$social_icon_group = peggi_select_add_admin_group(
				array(
					'name'   => 'qodef_team_member_social_icon_group' . $x,
					'title'  => esc_html__( 'Social Link ', 'peggi-core' ) . $x,
					'parent' => $meta_box
				)
			);
			
			$social_row1 = peggi_select_add_admin_row(
				array(
					'name'   => 'qodef_team_member_social_icon_row1' . $x,
					'parent' => $social_icon_group
				)
			);
			
			peggi_select_icon_collections()->getIconsMetaBoxOrOption(
				array(
					'label'            => esc_html__( 'Icon ', 'peggi-core' ) . $x,
					'parent'           => $social_row1,
					'name'             => 'qodef_team_member_social_icon_pack_' . $x,
					'defaul_icon_pack' => '',
					'type'             => 'meta-box',
					'field_type'       => 'simple'
				)
			);
			
			$social_row2 = peggi_select_add_admin_row(
				array(
					'name'   => 'qodef_team_member_social_icon_row2' . $x,
					'parent' => $social_icon_group
				)
			);
			
			peggi_select_create_meta_box_field(
				array(
					'type'            => 'textsimple',
					'label'           => esc_html__( 'Link', 'peggi-core' ),
					'name'            => 'qodef_team_member_social_icon_' . $x . '_link',
					'parent'          => $social_row2,
					'dependency' => array(
						'hide' => array(
							'qodef_team_member_social_icon_pack_'. $x  => ''
						)
					)
				)
			);
			
			peggi_select_create_meta_box_field(
				array(
					'type'            => 'selectsimple',
					'label'           => esc_html__( 'Target', 'peggi-core' ),
					'name'            => 'qodef_team_member_social_icon_' . $x . '_target',
					'options'         => peggi_select_get_link_target_array(),
					'parent'          => $social_row2,
					'dependency' => array(
						'hide' => array(
							'qodef_team_member_social_icon_' . $x . '_link'  => ''
						)
					)
				)
			);
		}
	}
	
	add_action( 'peggi_select_action_meta_boxes_map', 'peggi_core_map_team_single_meta', 46 );
}