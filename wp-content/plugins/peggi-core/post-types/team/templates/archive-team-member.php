<?php
get_header();
peggi_select_get_title();
do_action('peggi_select_action_before_main_content'); ?>
<div class="qodef-container qodef-default-page-template">
	<?php do_action('peggi_select_action_after_container_open'); ?>
	<div class="qodef-container-inner clearfix">
		<?php
			$peggi_taxonomy_id = get_queried_object_id();
			$peggi_taxonomy = !empty($peggi_taxonomy_id) ? get_term_by( 'id', $peggi_taxonomy_id, 'team-category' ) : '';
			$peggi_taxonomy_slug = !empty($peggi_taxonomy) ? $peggi_taxonomy->slug : '';
		
			peggi_core_get_team_category_list($peggi_taxonomy_slug);
		?>
	</div>
	<?php do_action('peggi_select_action_before_container_close'); ?>
</div>
<?php get_footer(); ?>
