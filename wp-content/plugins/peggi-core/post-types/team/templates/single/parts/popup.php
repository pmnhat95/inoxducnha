<div class="qodef-team-member-popup-holder">
	<div class="qodef-tmp-wrapper">
		<div class="qodef-tmp-inner">
			<div class="qodef-tmp-content" <?php echo peggi_select_get_inline_style( $content_styles ); ?>>
				<a class="qodef-tmp-close" href="javascript:void(0)"><?php echo peggi_select_icon_collections()->getSearchClose( 'linear_icons', false ); ?></a>
				<?php if ( ! empty ( $title ) ) { ?>
					<h3 class="qodef-tmp-title"><?php echo esc_html( $title ) ?></h3>
				<?php } ?>
				<?php if ( ( $separator ) === 'yes' ) { ?>
					<span class="qodef-tmp-separator"></span>
				<?php } ?>
				
				<?php echo do_shortcode( '[contact-form-7 id="' . $contact_form . '" html_class="' . $contact_form_style . '"]' ); ?>
			</div>
		</div>
	</div>
</div>
