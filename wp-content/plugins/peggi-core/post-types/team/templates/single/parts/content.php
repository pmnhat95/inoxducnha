
<div class="qodef-team-single-content">
	<div class="qodef-grid-row qodef-grid-huge-gutter">
		<div class="qodef-ts-details-holder qodef-grid-col-8">
			<div class="qodef-team-single-content-holder">
				<?php the_content(); ?>
			</div>
			<div class="qodef-ts-bio-holder">
				<?php if(!empty($position)) { ?>
					<div class="qodef-ts-info-row">
						<p class="qodef-ts-bio-info"><?php echo esc_html__('Position: ', 'peggi-core').' <span> '.esc_html($position). '</span>'; ?></p>
					</div>
				<?php } ?>
				<?php if(!empty($birth_date)) { ?>
					<div class="qodef-ts-info-row">
						<p class="qodef-ts-bio-info"><?php echo esc_html__('Date of birth: ', 'peggi-core').' <span> '.esc_html($birth_date) . '</span>'; ?></p>
					</div>
				<?php } ?>
				<?php if(!empty($education)) { ?>
					<div class="qodef-ts-info-row">
						<p class="qodef-ts-bio-info"><?php echo esc_html__('Education: ', 'peggi-core').' <span> '.esc_html($education). '</span>'; ?></p>
					</div>
				<?php } ?>
				<?php if(!empty($resume)) { ?>
					<div class="qodef-ts-info-row">
						<a itemprop="url" class="qodef-btn qodef-btn-medium qodef-btn-simple" href="<?php echo esc_url( $resume ); ?>" target="_blank" download>
							<span class="qodef-btn-text"><?php esc_html_e( 'View CV', 'peggi' ); ?></span>
						</a>
					</div>
				<?php } ?>
				
				<h4 class="qodef-ts-info-contact"><?php echo esc_html__('Contact', 'peggi-core')?></h4>
				<?php if(!empty($email)) { ?>
					<div class="qodef-ts-info-row">
						<p itemprop="email" class="qodef-ts-bio-info"><?php echo esc_html__('Email: ', 'peggi-core').' <span> '.sanitize_email(esc_html($email)). '</span>'; ?></p>
					</div>
				<?php } ?>
				<?php if(!empty($phone)) { ?>
					<div class="qodef-ts-info-row">
						<p class="qodef-ts-bio-info"><?php echo esc_html__('Phone: ', 'peggi-core').' <span> '.esc_html($phone). '</span>'; ?></p>
					</div>
				<?php } ?>
				<?php if(!empty($address)) { ?>
					<div class="qodef-ts-info-row">
						<p class="qodef-ts-bio-info"><?php echo esc_html__('Lives in: ', 'peggi-core').' <span> '.esc_html($address). '</span>'; ?></p>
					</div>
				<?php } ?>
				<div class="qodef-ts-info-row">
					<div class="qodef-ts-info-icon-holder">
						<?php foreach ($social_icons as $social_icon) {
							echo wp_kses_post($social_icon);
						} ?>
					</div>
				</div>
				
				<div class="qodef-ts-contact-button">
					<?php
					$button_params = array(
						'type'              => 'solid',
						'hover_animation'   => 'yes',
						'size'              => 'small',
						'link'              => '#',
						'text'              => esc_html__( 'Get In Touch', 'peggi' ),
					);
					
					echo peggi_select_return_button_html( $button_params );
					?>
				</div>
				
			</div>
		</div>
		<div class="qodef-ts-image-holder qodef-grid-col-4">
			<?php
			the_post_thumbnail();
			?>
		</div>
	</div>
	<?php if (!empty($selected_events)){ ?>
		<h4 class="qodef-ts-events-title"><?php echo esc_html__( 'All Events By', 'peggi-core' )?> <?php echo peggi_select_get_title_text(); ?> </h4>
		<?php
			peggi_core_get_team_member_event_list( $selected_events);
	} ?>
</div>