<div class="qodef-testimonials-holder clearfix <?php echo esc_attr($holder_classes); ?>">
    <div class="qodef-testimonials qodef-owl-slider" <?php echo peggi_select_get_inline_attrs( $data_attr ) ?>>

        <?php if ( $query_results->have_posts() ):
            while ( $query_results->have_posts() ) : $query_results->the_post();
                $title    = get_post_meta( get_the_ID(), 'qodef_testimonial_title', true );
                $text     = get_post_meta( get_the_ID(), 'qodef_testimonial_text', true );
                $author   = get_post_meta( get_the_ID(), 'qodef_testimonial_author', true );
                $position = get_post_meta( get_the_ID(), 'qodef_testimonial_author_position', true );
                $current_id = get_the_ID();
        ?>

                <div class="qodef-testimonial-content" id="qodef-testimonials-<?php echo esc_attr( $current_id ) ?>" <?php peggi_select_inline_style( $box_styles ); ?>>
                    <div class="qodef-testimonial-text-holder clearfix">
                        <?php if ( has_post_thumbnail() || ! empty( $author ) ) { ?>
                            <div class="qodef-testimonials-author-image-holder">
                                <?php if ( has_post_thumbnail() ) { ?>
                                    <div class="qodef-testimonial-image">
                                        <?php echo get_the_post_thumbnail( get_the_ID(), array( 183, 183 ) ); ?>
                                    </div>
                                <?php } ?>
                            </div>
	                        <div class="qodef-testimonials-text-holder">
		                        <?php if ( ! empty( $text ) ) { ?>
			                        <h6 class="qodef-testimonial-text"><?php echo esc_html( $text ); ?></h6>
		                        <?php } ?>
                                <?php if ( ! empty( $author ) ) { ?>
                                    <div class="qodef-testimonial-author">
                                        <h5 class="qodef-testimonials-author-name"><?php echo esc_html( $author ); ?></h5>
                                        <?php if ( ! empty( $position ) ) { ?>
                                            <p class="qodef-testimonials-author-job"><?php echo esc_html( $position ); ?></p>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
	                        </div>
                        <?php } ?>
                    </div>
                </div>

        <?php
            endwhile;
        else:
            echo esc_html__( 'Sorry, no posts matched your criteria.', 'peggi-core' );
        endif;

        wp_reset_postdata();

        ?>
    </div>
</div>