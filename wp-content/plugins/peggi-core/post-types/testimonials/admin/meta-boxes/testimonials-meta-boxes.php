<?php

if ( ! function_exists( 'peggi_core_map_testimonials_meta' ) ) {
	function peggi_core_map_testimonials_meta() {
		$testimonial_meta_box = peggi_select_create_meta_box(
			array(
				'scope' => array( 'testimonials' ),
				'title' => esc_html__( 'Testimonial', 'peggi-core' ),
				'name'  => 'testimonial_meta'
			)
		);
		
		peggi_select_create_meta_box_field(
			array(
				'name'        => 'qodef_testimonial_title',
				'type'        => 'text',
				'label'       => esc_html__( 'Title', 'peggi-core' ),
				'description' => esc_html__( 'Enter testimonial title', 'peggi-core' ),
				'parent'      => $testimonial_meta_box,
			)
		);
		
		peggi_select_create_meta_box_field(
			array(
				'name'        => 'qodef_testimonial_text',
				'type'        => 'text',
				'label'       => esc_html__( 'Text', 'peggi-core' ),
				'description' => esc_html__( 'Enter testimonial text', 'peggi-core' ),
				'parent'      => $testimonial_meta_box,
			)
		);
		
		peggi_select_create_meta_box_field(
			array(
				'name'        => 'qodef_testimonial_author',
				'type'        => 'text',
				'label'       => esc_html__( 'Author', 'peggi-core' ),
				'description' => esc_html__( 'Enter author name', 'peggi-core' ),
				'parent'      => $testimonial_meta_box,
			)
		);
		
		peggi_select_create_meta_box_field(
			array(
				'name'        => 'qodef_testimonial_author_position',
				'type'        => 'text',
				'label'       => esc_html__( 'Author Position', 'peggi-core' ),
				'description' => esc_html__( 'Enter author job position', 'peggi-core' ),
				'parent'      => $testimonial_meta_box,
			)
		);
	}
	
	add_action( 'peggi_select_action_meta_boxes_map', 'peggi_core_map_testimonials_meta', 95 );
}