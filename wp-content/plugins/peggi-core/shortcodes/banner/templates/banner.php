<div class="qodef-banner-holder <?php echo esc_attr($holder_classes); ?>">
    <div class="qodef-banner-image">
        <?php echo wp_get_attachment_image($image, 'full'); ?>
    </div>
    <div class="qodef-banner-text-holder" <?php echo peggi_select_get_inline_style($overlay_styles); ?>>
	    <div class="qodef-banner-text-outer">
		    <div class="qodef-banner-text-inner">
			    <div class="qodef-banner-text-holder-inner">
				    <?php if(!empty($title)) { ?>
					    <<?php echo esc_attr($title_tag); ?> class="qodef-banner-title" <?php echo peggi_select_get_inline_style($title_styles); ?>>
					        <?php echo wp_kses($title, array('span' => array('class' => true))); ?>
					    </<?php echo esc_attr($title_tag); ?>>
				    <?php } ?>
			        <?php if(!empty($subtitle)) { ?>
			            <<?php echo esc_attr($subtitle_tag); ?> class="qodef-banner-subtitle" <?php echo peggi_select_get_inline_style($subtitle_styles); ?>>
				            <?php echo esc_html($subtitle); ?>
			            </<?php echo esc_attr($subtitle_tag); ?>>
			        <?php } ?>
		
				    <?php if ( ! empty( $link ) && ! empty( $link_text ) ) {
					    $button_params = array(
						    'type'                   => 'simple',
						    'skin'                   => 'light',
						    'link'                   => $link,
						    'target'                 => $target,
						    'text'                   => $link_text,
					    );
					    echo peggi_select_get_button_html( $button_params );
				    } ?>
	            </div>
			</div>
		</div>
	</div>
	<?php if (!empty($link)) { ?>
        <a itemprop="url" class="qodef-banner-link" href="<?php echo esc_url($link); ?>" target="<?php echo esc_attr($target); ?>"></a>
    <?php } ?>
</div>