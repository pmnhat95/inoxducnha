(function($) {
	'use strict';
	
	var expandedGallery = {};
	qodef.modules.expandedGallery = expandedGallery;

	expandedGallery.qodefInitExpandedGallery = qodefInitExpandedGallery;


	expandedGallery.qodefOnDocumentReady = qodefOnDocumentReady;
	
	$(document).ready(qodefOnDocumentReady);
	
	/*
	 All functions to be called on $(document).ready() should be in this function
	 */
	function qodefOnDocumentReady() {
		qodefInitExpandedGallery();
	}

	/*
	 **	Init Expanded Gallery shortcode
	 */
	function qodefInitExpandedGallery(){
		var holder = $('.qodef-expanded-gallery');

		if(holder.length){
			holder.each(function() {
				var thisHolder = $(this),
					thisHolderImages = thisHolder.find('.qodef-eg-image');

				thisHolder.find('.qodef-eg-image:nth-child('+Math.ceil(thisHolderImages.length / 2)+')').addClass('qodef-eg-middle-item');

				thisHolder.appear(function() {
					thisHolder.find('.qodef-eg-middle-item').addClass('qodef-eg-show');

					setTimeout(function(){
						thisHolder.find('.qodef-eg-middle-item').prev().addClass('qodef-eg-show');
						thisHolder.find('.qodef-eg-middle-item').next().addClass('qodef-eg-show');
					},250);

					if (thisHolder.hasClass('qodef-eg-five')) {
						setTimeout(function(){
							thisHolder.find('.qodef-eg-middle-item').prev().prev().addClass('qodef-eg-show');
							thisHolder.find('.qodef-eg-middle-item').next().next().addClass('qodef-eg-show');
						},500);
					}
				}, {accX: 0, accY: qodefGlobalVars.vars.qodefElementAppearAmount});
			});
		}
	}
	
})(jQuery);