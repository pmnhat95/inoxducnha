<a itemprop="url" href="<?php echo esc_url($link); ?>" target="<?php echo esc_attr($target); ?>" <?php peggi_select_inline_style($button_styles); ?> <?php peggi_select_class_attribute($button_classes); ?> <?php echo peggi_select_get_inline_attrs($button_data); ?> <?php echo peggi_select_get_inline_attrs($button_custom_attrs); ?>>
    <span class="qodef-btn-text"><?php echo esc_html($text); ?></span>
    <?php echo peggi_select_icon_collections()->renderIcon($icon, $icon_pack); ?>
	<?php if ($hover_animation === 'yes') { ?>
		<span class="qodef-blob-btn-inner">
			<span class="qodef-blob-btn-blobs">
			<span class="qodef-blob-btn-blob"></span>
			<span class="qodef-blob-btn-blob"></span>
			<span class="qodef-blob-btn-blob"></span>
			<span class="qodef-blob-btn-blob"></span>
		</span>
    </span>
	<?php } ?>
</a>