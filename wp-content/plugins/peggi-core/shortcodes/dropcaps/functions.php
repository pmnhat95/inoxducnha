<?php

if ( ! function_exists( 'peggi_core_add_dropcaps_shortcodes' ) ) {
	function peggi_core_add_dropcaps_shortcodes( $shortcodes_class_name ) {
		$shortcodes = array(
			'PeggiCore\CPT\Shortcodes\Dropcaps\Dropcaps'
		);
		
		$shortcodes_class_name = array_merge( $shortcodes_class_name, $shortcodes );
		
		return $shortcodes_class_name;
	}
	
	add_filter( 'peggi_core_filter_add_vc_shortcode', 'peggi_core_add_dropcaps_shortcodes' );
}