<div class="qodef-ic-holder">
	<?php if ( ! empty( $carousel_items ) ) { ?>
		<?php $carousel_item_counter = 3; ?>
		<div class="qodef-ic-image-holder qodef-owl-slider" <?php echo peggi_select_get_inline_attrs($carousel_data); ?>>
			<?php foreach ( $carousel_items as $carousel_item ): ?>
				<?php if ( isset( $carousel_item['image'] ) ) { ?>
					<?php
						$item_style   = array();
						$item_style[] = 'background-image: url(' . wp_get_attachment_url( $carousel_item['image'] ) . ')';
					?>

					<?php if ($carousel_item_counter%3 !== 2) { ?>
				<div class="qodef-ic-item-image-wrapper">
					<?php } ?>

						<div class="qodef-ic-item-image qodef-ic-item-image-background <?php if ($carousel_item_counter%3 !== 0) { echo 'qodef-ic-item-image-wrapper-dual'; } ?>"  <?php echo peggi_select_get_inline_style($item_style); ?>>
							<a  href="<?php echo esc_url($carousel_item['link']); ?>" target="<?php echo esc_attr($carousel_item['link_target']); ?>"></a>
						</div>

						<div class="qodef-ic-item-image <?php if ($carousel_item_counter%3 !== 0) { echo 'qodef-ic-item-image-wrapper-dual'; } ?>">
							<?php echo wp_get_attachment_image( $carousel_item['image'], 'full' ); ?>
						</div>
					<?php if ($carousel_item_counter%3 !== 1 ) { ?>
						</div>
					<?php } ?>

					<?php $carousel_item_counter++; ?>
				<?php } ?>
			<?php endforeach; ?>
		</div>
	<?php } ?>
</div>