<?php
namespace PeggiCore\CPT\Shortcodes\ImagesCarousel;

use PeggiCore\Lib;

class ImagesCarousel implements Lib\ShortcodeInterface {
	private $base;
	
	function __construct() {
		$this->base = 'qodef_images_carousel_showcase';
		
		add_action( 'vc_before_init', array( $this, 'vcMap' ) );
	}
	
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		if ( function_exists( 'vc_map' ) ) {
			vc_map(
				array(
					'name'     => esc_html__( 'Images Carousel', 'peggi-core' ),
					'base'     => $this->getBase(),
					'category' => esc_html__( 'by PEGGI', 'peggi-core' ),
					'icon'     => 'icon-wpb-images-carousel-showcase extended-custom-icon',
					'params'   => array(
						array(
							'type'        => 'dropdown',
							'param_name'  => 'slider_loop',
							'heading'     => esc_html__( 'Enable Slider Loop', 'peggi-core' ),
							'value'       => array_flip( peggi_select_get_yes_no_select_array( false, true ) ),
							'save_always' => true
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'slider_autoplay',
							'heading'     => esc_html__( 'Enable Slider Autoplay', 'peggi-core' ),
							'value'       => array_flip( peggi_select_get_yes_no_select_array( false, true ) ),
							'save_always' => true
						),
						array(
							'type'        => 'textfield',
							'param_name'  => 'slider_speed',
							'heading'     => esc_html__( 'Slide Duration', 'peggi-core' ),
							'description' => esc_html__( 'Default value is 5000 (ms)', 'peggi-core' )
						),
						array(
							'type'        => 'textfield',
							'param_name'  => 'slider_speed_animation',
							'heading'     => esc_html__( 'Slide Animation Duration', 'peggi-core' ),
							'description' => esc_html__( 'Speed of slide animation in milliseconds. Default value is 600.', 'peggi-core' )
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'slider_navigation',
							'heading'     => esc_html__( 'Enable Slider Navigation', 'peggi-core' ),
							'value'       => array_flip( peggi_select_get_yes_no_select_array( false, true ) ),
							'save_always' => true
						),
						array(
							'type'        => 'dropdown',
							'param_name'  => 'slider_pagination',
							'heading'     => esc_html__( 'Enable Slider Pagination', 'peggi-core' ),
							'value'       => array_flip( peggi_select_get_yes_no_select_array( false, true ) ),
							'save_always' => true
						),
						array(
							'type'       => 'param_group',
							'param_name' => 'carousel_items',
							'heading'    => esc_html__( 'Carousel Items', 'peggi-core' ),
							'params'     => array(
								array(
									'type'       => 'textfield',
									'param_name' => 'link',
									'heading'    => esc_html__( 'Link', 'peggi-core' )
								),
								array(
									'type'       => 'dropdown',
									'param_name' => 'link_target',
									'heading'    => esc_html__( 'Link Target', 'peggi-core' ),
									'value'      => array_flip( peggi_select_get_link_target_array() ),
									'dependency' => array( 'element' => 'link', 'not_empty' => true )
								),
								array(
									'type'        => 'attach_image',
									'param_name'  => 'image',
									'heading'     => esc_html__( 'Image', 'peggi-core' ),
									'description' => esc_html__( 'Select image from media library', 'peggi-core' )
								)
							)
						)
					)
				)
			);
		}
	}
	
	public function render( $atts, $content = null ) {
		$args   = array(
			'slider_loop'             => 'yes',
			'slider_autoplay'         => 'yes',
			'slider_speed'            => '5000',
			'slider_speed_animation'  => '600',
			'slider_navigation'       => '',
			'slider_pagination'       => '',
			'carousel_items' 		  => ''
		);
		$params = shortcode_atts( $args, $atts );
		
		$params['carousel_items']      = json_decode( urldecode( $params['carousel_items'] ), true );
		$params['carousel_data']  	   = $this->getSliderData( $params );
		
		$html = peggi_core_get_shortcode_module_template_part( 'templates/images-carousel', 'images-carousel', '', $params );
		
		return $html;
	}

	private function getSliderData( $params ) {
		$slider_data = array();
		
		$slider_data['data-number-of-items']        = '4';
		$slider_data['data-slider-margin']			= '30';
		$slider_data['data-enable-loop']            = ! empty( $params['slider_loop'] ) ? $params['slider_loop'] : '';
		$slider_data['data-enable-autoplay']        = ! empty( $params['slider_autoplay'] ) ? $params['slider_autoplay'] : '';
		$slider_data['data-slider-speed']           = ! empty( $params['slider_speed'] ) ? $params['slider_speed'] : '5000';
		$slider_data['data-slider-speed-animation'] = ! empty( $params['slider_speed_animation'] ) ? $params['slider_speed_animation'] : '600';
		$slider_data['data-enable-navigation']      = ! empty( $params['slider_navigation'] ) ? $params['slider_navigation'] : '';
		$slider_data['data-enable-pagination']      = ! empty( $params['slider_pagination'] ) ? $params['slider_pagination'] : '';
		
		return $slider_data;
	}
}