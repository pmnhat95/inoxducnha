(function($) {
	'use strict';
	
	var sectionTitle = {};
	qodef.modules.sectionTitle = sectionTitle;
	
	sectionTitle.qodefInitSectionTitle = qodefInitSectionTitle;
	
	sectionTitle.qodefOnDocumentReady = qodefOnDocumentReady;
	
	$(document).ready(qodefOnDocumentReady);
	
	/*
	 All functions to be called on $(document).ready() should be in this function
	 */
	function qodefOnDocumentReady() {
		qodefInitSectionTitle();
	}
	
	function qodefInitSectionTitle() {
		var holder = $('.qodef-section-title-holder.qodef-st-animation');
		
		if(holder.length) {
			holder.each(function(){
				var thisHolder = $(this);
				
				thisHolder.appear(function(){
					thisHolder.addClass('qodef-separator-appeared');
				},{accX: 0, accY: qodefGlobalVars.vars.qodefElementAppearAmount});
			});
		}
	}
	
})(jQuery);