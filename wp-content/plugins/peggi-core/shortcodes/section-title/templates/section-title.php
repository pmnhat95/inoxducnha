<div class="qodef-section-title-holder <?php echo esc_attr($holder_classes); ?>" <?php echo peggi_select_get_inline_style($holder_styles); ?>>
	<div class="qodef-st-inner">
		<?php if(!empty($title)) { ?>
			<<?php echo esc_attr($title_tag); ?> class="qodef-st-title" <?php echo peggi_select_get_inline_style($title_styles); ?>>
				<?php echo wp_kses($title, array('br' => true, 'span' => array('class' => true))); ?>
			</<?php echo esc_attr($title_tag); ?>>
		<?php } ?>
		<span class="qodef-st-separator"></span>
		<?php if(!empty($text)) { ?>
			<p class="qodef-st-text" <?php echo peggi_select_get_inline_style($text_styles); ?>>
				<?php echo wp_kses($text, array('br' => true)); ?>
			</p>
		<?php } ?>
		<?php if ( ! empty( $link ) && ! empty( $link_text ) ) {
			$button_params = array(
				'type'                   => 'simple',
				'link'                   => $link,
				'target'                 => $target,
				'text'                   => $link_text,
			);
			echo peggi_select_get_button_html( $button_params );
		} ?>
	</div>
</div>