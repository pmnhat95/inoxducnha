<?php

namespace PeggiCore\Lib;

/**
 * interface PostTypeInterface
 * @package PeggiCore\Lib;
 */
interface PostTypeInterface {
	/**
	 * @return string
	 */
	public function getBase();
	
	/**
	 * Registers custom post type with WordPress
	 */
	public function register();
}