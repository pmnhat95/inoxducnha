<?php
/**
 * Single Event Template
 * A single event. This displays the event title, description, meta, and
 * optionally, the Google map for the event.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/single-event.php
 *
 * @package TribeEventsCalendar
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$events_label_singular = tribe_get_event_label_singular();
$events_label_plural   = tribe_get_event_label_plural();

global $post;

$event_id = get_the_ID();

$price_class = '';
if ( tribe_get_cost( null, true ) === 'Free' ) {
	$price_class = 'qodef-free';
}
?>

<div id="tribe-events-content" class="tribe-events-single qodef-tribe-events-single">
	<!-- Notices -->
	<?php tribe_the_notices() ?>
	
	<div class="qodef-events-single-main-info clearfix">
		<div class="qodef-events-single-date-holder">
			<div class="qodef-events-single-date-inner">
				<h5 class="qodef-events-single-date-day">
					<?php echo tribe_get_start_date( null, true, 'd' ); ?>
				</h5>
				
				<span class="qodef-events-single-date-month">
					<?php echo tribe_get_start_date( null, true, 'M' ); ?>
				</span>
			</div>
		</div>
		<div class="qodef-events-single-title-holder">
			<h2 class="qodef-events-single-title"><?php the_title(); ?></h2>
			<div class="qodef-events-single-date">
				<span class="qodef-events-single-info-icon">
					<?php echo peggi_select_icon_collections()->renderIcon( 'lnr-clock', 'linear_icons' ); ?>
				</span>
				<?php echo tribe_events_event_schedule_details( $event_id ); ?>
			</div>
			<?php if ( tribe_get_cost( $event_id ) ) { ?>
				<div class="qodef-events-single-cost <?php echo esc_html( $price_class ) ?>">
					<?php echo tribe_get_cost( null, true ); ?>
				</div>
			<?php } ?>
		</div>
	</div>
	
	<div class="qodef-events-single-main-content">
		<div class="qodef-grid-row qodef-events-single-media">
			<div class="qodef-events-single-featured-image qodef-grid-col-6">
				<?php echo tribe_event_featured_image( $event_id, 'full', false ); ?>
			</div>
			<div class="qodef-events-single-map qodef-grid-col-6">
				<?php tribe_get_template_part( 'modules/meta/map' ); ?>
			</div>
		</div>
		<div class="qodef-events-single-content-holder">
			<?php do_action( 'tribe_events_single_event_before_the_content' ) ?>
			
			<?php the_content(); ?>
			
			<?php do_action( 'tribe_events_single_event_after_the_content' ) ?>
		</div>
	</div>
	
	<div class="qodef-events-single-meta">
		<?php do_action( 'tribe_events_single_event_before_the_meta' ); ?>
		<h4><?php esc_html_e( 'Event Details', 'peggi' ); ?></h4>
		
		<div class="qodef-events-single-meta-holder qodef-grid-row">
			<div class="qodef-grid-col-4">
				<div class="qodef-events-single-meta-item">
					<span class="qodef-events-single-meta-icon">
						<?php echo peggi_select_icon_collections()->renderIcon( 'lnr-calendar-full', 'linear_icons' ); ?>
					</span>
					<span class="qodef-events-single-meta-label"><?php esc_html_e( 'Date:', 'peggi' ); ?></span>
					<span class="qodef-events-single-meta-value">
						<?php echo tribe_events_event_schedule_details(); ?>
					</span>
				</div>
				
				<div class="qodef-events-single-meta-item">
					<span class="qodef-events-single-meta-icon">
						<?php echo peggi_select_icon_collections()->renderIcon( 'lnr-clock', 'linear_icons' ); ?>
					</span>
					<span class="qodef-events-single-meta-label"><?php esc_html_e( 'Time:', 'peggi' ); ?></span>
					<span class="qodef-events-single-meta-value">
						<?php echo tribe_get_start_time(); ?> - <?php echo tribe_get_end_time(); ?>
					</span>
				</div>
				
				<?php if ( tribe_has_venue() ) : ?>
					<div class="qodef-events-single-meta-item">
						<span class="qodef-events-single-meta-icon">
							<?php echo peggi_select_icon_collections()->renderIcon( 'lnr-apartment', 'linear_icons' ); ?>
						</span>
						<span class="qodef-events-single-meta-label"><?php esc_html_e( 'Venue:', 'peggi' ); ?></span>
						<span class="qodef-events-single-meta-value">
							<?php echo esc_html( tribe_get_venue() ); ?>
						</span>
					</div>
					
					<?php if ( tribe_address_exists() ) : ?>
						<div class="qodef-events-single-meta-item">
							<span class="qodef-events-single-meta-icon">
								<?php echo peggi_select_icon_collections()->renderIcon( ' lnr-map-marker', 'linear_icons' ); ?>
							</span>
							<span class="qodef-events-single-meta-label"><?php esc_html_e( 'Address:', 'peggi' ); ?></span>
							<span class="qodef-events-single-meta-value">
								<?php echo tribe_get_address(); ?>
							</span>
						</div>
					<?php endif; ?>
				<?php endif; ?>
			</div>
			
			<div class="qodef-grid-col-4">
				<?php if ( tribe_has_organizer() ) : ?>
					<?php
					$organizer_ids = tribe_get_organizer_ids();
					foreach ( $organizer_ids as $organizer ) { ?>
						<div class="qodef-events-single-meta-item">
							<span class="qodef-events-single-meta-icon">
								<?php echo peggi_select_icon_collections()->renderIcon( 'lnr-user', 'linear_icons' ); ?>
							</span>
							<span class="qodef-events-single-meta-label"><?php esc_html_e( 'Organizer Name:', 'peggi' ); ?></span>
							<span class="qodef-events-single-meta-value">
                                <?php echo tribe_get_organizer_link( $organizer ); ?>
							</span>
						</div>
						<?php
					}
					?>
				<?php endif; ?>
				
				<?php if ( tribe_get_organizer_phone() ) : ?>
					<div class="qodef-events-single-meta-item">
						<span class="qodef-events-single-meta-icon">
							<?php echo peggi_select_icon_collections()->renderIcon( 'lnr-phone-handset', 'linear_icons' ); ?>
						</span>
						<span class="qodef-events-single-meta-label"><?php esc_html_e( 'Phone:', 'peggi' ); ?></span>
						<span class="qodef-events-single-meta-value">
							<?php echo esc_html( tribe_get_organizer_phone() ); ?>
						</span>
					</div>
				<?php endif; ?>
				
				<?php if ( tribe_get_organizer_email() ) : ?>
					<div class="qodef-events-single-meta-item">
						<span class="qodef-events-single-meta-icon">
							<?php echo peggi_select_icon_collections()->renderIcon( 'lnr-envelope', 'linear_icons' ); ?>
						</span>
						<span class="qodef-events-single-meta-label"><?php esc_html_e( 'Email:', 'peggi' ); ?></span>
						<span class="qodef-events-single-meta-value">
							<a href="mailto: <?php echo tribe_get_organizer_email(); ?>">
								<?php echo esc_html( tribe_get_organizer_email() ); ?>
							</a>
						</span>
					</div>
				<?php endif; ?>
				
				<?php if ( tribe_get_organizer_website_link() ) : ?>
					<div class="qodef-events-single-meta-item">
						<span class="qodef-events-single-meta-icon">
							<?php echo peggi_select_icon_collections()->renderIcon( 'lnr-earth', 'linear_icons' ); ?>
						</span>
						<span class="qodef-events-single-meta-label"><?php esc_html_e( 'Website:', 'peggi' ); ?></span>
						<span class="qodef-events-single-meta-value">
							<a target="_blank" href="<?php echo tribe_get_organizer_website_url(); ?>">
								<?php echo tribe_get_organizer_website_url(); ?>
							</a>
						</span>
					</div>
				<?php endif; ?>
			</div>
		</div>
		
		<div class="qodef-events-single-navigation-holder clearfix">
			<div class="qodef-events-single-prev-event">
				<?php
				$prev_event = Tribe__Events__Main::instance()->get_closest_event( $post, 'previous' );
				if ( $prev_event !== null ) {
					?>
					<a itemprop="url" class="qodef-events-nav-image" href="<?php echo esc_attr( tribe_get_event_link( $prev_event ) ); ?>">
						<?php echo get_the_post_thumbnail( $prev_event, 'peggi_select_image_square' ); ?>
					</a>
					<span class="qodef-events-nav-text">
						<span class="qodef-events-nav-label"><?php esc_html_e( 'Previous', 'peggi' ) ?></span>
						<?php tribe_the_prev_event_link( '%title%' ); ?>
					</span>
				<?php } ?>
			</div>
			
			<div class="qodef-events-single-next-event">
				<?php
				$next_event = Tribe__Events__Main::instance()->get_closest_event( $post, 'next' );
				if ( $next_event !== null ) {
					?>
					<span class="qodef-events-nav-text">
						<span class="qodef-events-nav-label"><?php esc_html_e( 'Next', 'peggi' ) ?></span>
						<?php tribe_the_next_event_link( '%title%' ); ?>
					</span>
					<a itemprop="url" class="qodef-events-nav-image" href="<?php echo esc_attr( tribe_get_event_link( $next_event ) ); ?>">
						<?php echo get_the_post_thumbnail( $next_event, 'peggi_select_image_square' ); ?>
					</a>
				<?php } ?>
			</div>
		</div>
		
		<?php do_action( 'tribe_events_single_event_after_the_meta' ); ?>
	</div>
</div>
