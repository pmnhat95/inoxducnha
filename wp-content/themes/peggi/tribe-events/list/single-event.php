<?php
/**
 * List View Single Event
 * This file contains one event in the list view
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/list/single-event.php
 *
 * @version 4.6.3
 *
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

// Setup an array of venue details for use later in the template
$venue_details = tribe_get_venue_details();

// The address string via tribe_get_venue_details will often be populated even when there's
// no address, so let's get the address string on its own for a couple of checks below.
$venue_address = tribe_get_address();

// Venue
$has_venue_address = ( ! empty( $venue_details['address'] ) ) ? ' location' : '';

// Organizer
$organizer = tribe_get_organizer();

$price_class = '';
if ( tribe_get_cost( null, true ) == 'Free' ) {
	$price_class = 'qodef-free';
}
?>

<div class="qodef-events-list-item-holder qodef-grid-row">
	<div class="qodef-grid-col-6">
		<div class="qodef-events-list-item-image-holder">
			<a href="<?php echo esc_url( tribe_get_event_link() ); ?>">
				<?php the_post_thumbnail( 'large' ); ?>
				
				<div class="qodef-events-list-item-date-holder">
					<div class="qodef-events-list-item-date-inner">
						<span class="qodef-events-list-item-date-day">
							<?php echo tribe_get_start_date( null, true, 'd' ); ?>
						</span>
						<span class="qodef-events-list-item-date-month">
							<?php echo tribe_get_start_date( null, true, 'M' ); ?>
						</span>
					</div>
				</div>
			</a>
		</div>
	</div>

	<div class="qodef-grid-col-6">
		<div class="qodef-events-list-item-content">
			<div class="qodef-events-list-item-title-holder">
				
				<!-- Event Title -->
				<?php do_action( 'tribe_events_before_the_event_title' ) ?>
				
				<h4 class="qodef-events-list-item-title">
					<a class="tribe-event-url" href="<?php echo esc_url( tribe_get_event_link() ); ?>" title="<?php the_title_attribute() ?>" rel="bookmark">
						<?php the_title() ?>
					</a>
				</h4>
				
				<?php if ( tribe_get_cost() ) : ?>
					<div class="qodef-events-list-item-price <?php echo esc_html($price_class);?>">
						<span class="ticket-cost"><?php echo tribe_get_cost( null, true ); ?></span>
						<?php
						/**
						 * Runs after cost is displayed in list style views
						 *
						 * @since 4.5
						 */
						do_action( 'tribe_events_inside_cost' )
						?>
					</div>
				<?php endif; ?>

				<?php do_action('tribe_events_after_the_event_title') ?>
			</div>

			<?php do_action('tribe_events_before_the_meta') ?>
		
			<div class="qpdef-events-list-item-meta">
				<div class="qodef-events-single-meta-item">
						<span class="qodef-events-single-meta-icon">
							<?php echo peggi_select_icon_collections()->renderIcon('lnr-calendar-full', 'linear_icons'); ?>
						</span>
					<span class="qodef-events-single-meta-label"><?php esc_html_e('Date:', 'peggi'); ?></span>
					<span class="qodef-events-single-meta-value">
							<?php echo tribe_events_event_schedule_details(); ?>
						</span>
				</div>
				
				<div class="qodef-events-single-meta-item">
						<span class="qodef-events-single-meta-icon">
							<?php echo peggi_select_icon_collections()->renderIcon('icon-clock', 'simple_line_icons'); ?>
						</span>
					<span class="qodef-events-single-meta-label"><?php esc_html_e('Time:', 'peggi'); ?></span>
					<span class="qodef-events-single-meta-value">
							<?php echo tribe_get_start_time(); ?> - <?php echo tribe_get_end_time(); ?>
						</span>
				</div>
				
				<?php if(tribe_has_venue()) : ?>
					<div class="qodef-events-single-meta-item">
							<span class="qodef-events-single-meta-icon">
								<?php echo peggi_select_icon_collections()->renderIcon('lnr-apartment', 'linear_icons'); ?>
							</span>
						<span class="qodef-events-single-meta-label"><?php esc_html_e('Venue:', 'peggi'); ?></span>
						<span class="qodef-events-single-meta-value">
								<?php echo esc_html(tribe_get_venue()); ?>
							</span>
					</div>
					
					<?php if(tribe_address_exists()) : ?>
						<div class="qodef-events-single-meta-item">
								<span class="qodef-events-single-meta-icon">
									<?php echo peggi_select_icon_collections()->renderIcon('lnr-map-marker', 'linear_icons'); ?>
								</span>
							<span class="qodef-events-single-meta-label"><?php esc_html_e('Address:', 'peggi'); ?></span>
							<span class="qodef-events-single-meta-value">
									<?php echo tribe_get_address(); ?>
								</span>
						</div>
					<?php endif; ?>
				<?php endif; ?>
			</div>

			<?php do_action('tribe_events_after_the_meta') ?>
		
			<?php if ( tribe_events_get_the_excerpt() ) : ?>

				<?php do_action('tribe_events_before_the_content') ?>
				
				<div class="tribe-events-list-event-description tribe-events-content description entry-summary">
					<?php echo tribe_events_get_the_excerpt( null, wp_kses_allowed_html( 'post' ) ); ?>
					<a href="<?php echo esc_url( tribe_get_event_link() ); ?>" class="tribe-events-read-more" rel="bookmark"><?php esc_html_e( 'Find out more', 'peggi' ) ?> &raquo;</a>
				</div><!-- .tribe-events-list-event-description -->

				<?php do_action('tribe_events_after_the_content'); ?>

			<?php endif; ?>
		</div>
	</div>
</div>