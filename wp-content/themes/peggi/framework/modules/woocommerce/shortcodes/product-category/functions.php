<?php
if(!function_exists('peggi_select_add_product_category_shortcode')) {
	function peggi_select_add_product_category_shortcode($shortcodes_class_name) {
		$shortcodes = array(
			'PeggiCore\CPT\Shortcodes\ProductCategory\ProductCategory',
		);

		$shortcodes_class_name = array_merge($shortcodes_class_name, $shortcodes);

		return $shortcodes_class_name;
	}

	if(peggi_select_core_plugin_installed()) {
		add_filter('peggi_core_filter_add_vc_shortcode', 'peggi_select_add_product_category_shortcode');
	}
}

if ( ! function_exists( 'peggi_select_add_product_category_into_shortcodes_list' ) ) {
    function peggi_select_add_product_category_into_shortcodes_list( $woocommerce_shortcodes ) {
        $woocommerce_shortcodes[] = 'qodef_product_category';

        return $woocommerce_shortcodes;
    }

    add_filter( 'peggi_select_filter_woocommerce_shortcodes_list', 'peggi_select_add_product_category_into_shortcodes_list' );
}