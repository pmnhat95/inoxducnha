<div class="qodef-category-holder <?php echo esc_html($skin) ?>">
	<?php  if ( ! empty( $custom_link ) ) { ?>
		<a itemprop="url" class="qodef-pc-custom-link" href="<?php echo esc_url( $custom_link ); ?>" target="<?php echo esc_attr( $custom_link_target ); ?>"></a>
	<?php } ?>
    <div class="qodef-category-image">
        <?php echo wp_get_attachment_image($params['category_image'], $category_image_size); ?>
        <div class="qodef-category-text-wrapper">
            <div class="qodef-category-text-holder">
                <div class="qodef-category-text">
                    <<?php echo esc_html($params['title_tag']); ?> class="qodef-category-title"> <?php echo esc_html($params['category']) ?> </<?php echo esc_html($params['title_tag']); ?>>
                </div>
            </div>
        </div>
    </div>
</div>