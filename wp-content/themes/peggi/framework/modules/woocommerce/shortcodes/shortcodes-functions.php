<?php

if ( ! function_exists( 'peggi_select_include_woocommerce_shortcodes' ) ) {
	function peggi_select_include_woocommerce_shortcodes() {
		foreach ( glob( SELECT_FRAMEWORK_MODULES_ROOT_DIR . '/woocommerce/shortcodes/*/load.php' ) as $shortcode_load ) {
			include_once $shortcode_load;
		}
	}
	
	if ( peggi_select_core_plugin_installed() ) {
		add_action( 'peggi_core_action_include_shortcodes_file', 'peggi_select_include_woocommerce_shortcodes' );
	}
}
