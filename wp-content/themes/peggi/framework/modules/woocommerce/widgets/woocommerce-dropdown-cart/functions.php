<?php

if ( ! function_exists( 'peggi_select_register_woocommerce_dropdown_cart_widget' ) ) {
	/**
	 * Function that register dropdown cart widget
	 */
	function peggi_select_register_woocommerce_dropdown_cart_widget( $widgets ) {
		$widgets[] = 'PeggiSelectClassWoocommerceDropdownCart';
		
		return $widgets;
	}
	
	add_filter( 'peggi_select_filter_register_widgets', 'peggi_select_register_woocommerce_dropdown_cart_widget' );
}

if ( ! function_exists( 'peggi_select_get_dropdown_cart_icon_class' ) ) {
	/**
	 * Returns dropdow cart icon class
	 */
	function peggi_select_get_dropdown_cart_icon_class() {
		$classes = array(
			'qodef-header-cart'
		);
		
		$classes[] = peggi_select_get_icon_sources_class( 'dropdown_cart', 'qodef-header-cart' );
		
		return $classes;
	}
}