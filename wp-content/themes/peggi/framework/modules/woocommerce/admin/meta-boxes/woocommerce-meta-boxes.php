<?php

if ( ! function_exists( 'peggi_select_map_woocommerce_meta' ) ) {
	function peggi_select_map_woocommerce_meta() {
		
		$woocommerce_meta_box = peggi_select_create_meta_box(
			array(
				'scope' => array( 'product' ),
				'title' => esc_html__( 'Product Meta', 'peggi' ),
				'name'  => 'woo_product_meta'
			)
		);
		
		peggi_select_create_meta_box_field(
			array(
				'name'        => 'qodef_product_featured_image_size',
				'type'        => 'select',
				'label'       => esc_html__( 'Dimensions for Product List Shortcode', 'peggi' ),
				'description' => esc_html__( 'Choose image layout when it appears in Select Product List - Masonry layout shortcode', 'peggi' ),
				'options'     => array(
					''                   => esc_html__( 'Default', 'peggi' ),
					'small'              => esc_html__( 'Small', 'peggi' ),
					'large-width'        => esc_html__( 'Large Width', 'peggi' ),
					'large-height'       => esc_html__( 'Large Height', 'peggi' ),
					'large-width-height' => esc_html__( 'Large Width Height', 'peggi' )
				),
				'parent'      => $woocommerce_meta_box
			)
		);
		
		peggi_select_create_meta_box_field(
			array(
				'name'          => 'qodef_show_title_area_woo_meta',
				'type'          => 'select',
				'default_value' => '',
				'label'         => esc_html__( 'Show Title Area', 'peggi' ),
				'description'   => esc_html__( 'Disabling this option will turn off page title area', 'peggi' ),
				'options'       => peggi_select_get_yes_no_select_array(),
				'parent'        => $woocommerce_meta_box
			)
		);
		
		peggi_select_create_meta_box_field(
			array(
				'name'          => 'qodef_show_new_sign_woo_meta',
				'type'          => 'yesno',
				'default_value' => 'no',
				'label'         => esc_html__( 'Show New Sign', 'peggi' ),
				'description'   => esc_html__( 'Enabling this option will show new sign mark on product', 'peggi' ),
				'parent'        => $woocommerce_meta_box
			)
		);
	}
	
	add_action( 'peggi_select_action_meta_boxes_map', 'peggi_select_map_woocommerce_meta', 99 );
}