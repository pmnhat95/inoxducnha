<?php

if ( ! function_exists( 'peggi_select_like' ) ) {
	/**
	 * Returns PeggiSelectClassLike instance
	 *
	 * @return PeggiSelectClassLike
	 */
	function peggi_select_like() {
		return PeggiSelectClassLike::get_instance();
	}
}

function peggi_select_get_like() {
	
	echo wp_kses( peggi_select_like()->add_like(), array(
		'span' => array(
			'class'       => true,
			'aria-hidden' => true,
			'style'       => true,
			'id'          => true
		),
		'i'    => array(
			'class' => true,
			'style' => true,
			'id'    => true
		),
		'a'    => array(
			'href'  => true,
			'class' => true,
			'id'    => true,
			'title' => true,
			'style' => true
		)
	) );
}