<?php

if ( ! function_exists( 'peggi_select_register_header_centered_type' ) ) {
	/**
	 * This function is used to register header type class for header factory file
	 */
	function peggi_select_register_header_centered_type( $header_types ) {
		$header_type = array(
			'header-centered' => 'PeggiSelectNamespace\Modules\Header\Types\HeaderCentered'
		);
		
		$header_types = array_merge( $header_types, $header_type );
		
		return $header_types;
	}
}

if ( ! function_exists( 'peggi_select_init_register_header_centered_type' ) ) {
	/**
	 * This function is used to wait header-function.php file to init header object and then to init hook registration function above
	 */
	function peggi_select_init_register_header_centered_type() {
		add_filter( 'peggi_select_filter_register_header_type_class', 'peggi_select_register_header_centered_type' );
	}
	
	add_action( 'peggi_select_action_before_header_function_init', 'peggi_select_init_register_header_centered_type' );
}

if ( ! function_exists( 'peggi_select_header_centered_per_page_custom_styles' ) ) {
	/**
	 * Return header per page styles
	 */
	function peggi_select_header_centered_per_page_custom_styles( $style, $class_prefix, $page_id ) {
		$header_area_style    = array();
		$header_area_selector = array( $class_prefix . '.qodef-header-centered .qodef-logo-area .qodef-logo-wrapper' );
		
		$logo_area_logo_padding = get_post_meta( $page_id, 'qodef_logo_wrapper_padding_header_centered_meta', true );
		
		if ( $logo_area_logo_padding !== '' ) {
			$header_area_style['padding'] = $logo_area_logo_padding;
		}
		
		$current_style = '';
		
		if ( ! empty( $header_area_style ) ) {
			$current_style .= peggi_select_dynamic_css( $header_area_selector, $header_area_style );
		}
		
		$current_style = $current_style . $style;
		
		return $current_style;
	}
	
	add_filter( 'peggi_select_filter_add_header_page_custom_style', 'peggi_select_header_centered_per_page_custom_styles', 10, 3 );
}

if ( ! function_exists( 'peggi_select_register_header_centered_widgets' ) ) {
	/**
	 * Registers additional widget areas for this header type
	 */
	function peggi_select_register_header_centered_widgets() {
		register_sidebar(
			array(
				'id'            => 'centered_header_left',
				'name'          => esc_html__( 'Centered Header Left', 'peggi' ),
				'description'   => esc_html__( 'This widget area is rendered on the left of the centered header logo area.', 'peggi' ),
				'before_widget' => '<div class="%2$s qodef-centered-menu-left-widget">',
				'after_widget'  => '</div>',
				'before_title'  => '<h5 class="qodef-widget-title">',
				'after_title'   => '</h5>'
			)
		);

		register_sidebar(
			array(
				'id'            => 'centered_header_right',
				'name'          => esc_html__( 'Centered Header Right', 'peggi' ),
				'description'   => esc_html__( 'This widget area is rendered on the right of the centered header logo area.', 'peggi' ),
				'before_widget' => '<div class="%2$s qodef-centered-menu-right-widget">',
				'after_widget'  => '</div>',
				'before_title'  => '<h5 class="qodef-widget-title">',
				'after_title'   => '</h5>'
			)
		);
	}

	if ( peggi_select_check_is_header_type_enabled( 'header-centered' ) ) {
		add_action( 'widgets_init', 'peggi_select_register_header_centered_widgets' );
	}
}