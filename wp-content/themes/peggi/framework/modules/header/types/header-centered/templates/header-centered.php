<?php do_action('peggi_select_action_before_page_header'); ?>

<header class="qodef-page-header">
	<?php do_action('peggi_select_action_after_page_header_html_open'); ?>
	
    <div class="qodef-logo-area">
	    <?php do_action( 'peggi_select_action_after_header_logo_area_html_open' ); ?>
	    
        <div class="qodef-grid">
			
            <div class="qodef-vertical-align-containers">
                <div class="qodef-position-left"><!--
                 --><div class="qodef-position-left-inner">
                        <div class="qodef-centered-widget-holder widget woocommerce widget_product_search">
                            <?php get_product_search_form(); ?>
                        </div>
                    </div>
                </div>
                <div class="qodef-position-center"><!--
                 --><div class="qodef-position-center-inner">
                    <div class="cty-ducnha-title">Công Ty TNHH TMDV Kim Nguyên</div>
                        <?php if(!$hide_logo) {
                            peggi_select_get_logo();
                        } ?>
                    </div>
                </div>
                <div class="qodef-position-right"><!--
                 --><div class="qodef-position-right-inner">
                        <div class="qodef-centered-widget-holder">
                            <span>Giỏ hàng của bạn</span>
                            <?php peggi_select_get_centered_header_widget_menu_area_right(); ?>
                        </div>
                    </div>
                </div>
            </div>
	            
        </div>
    </div>
	
    <?php if($show_fixed_wrapper) : ?>
        <div class="qodef-fixed-wrapper">
    <?php endif; ?>
	        
    <div class="qodef-menu-area">
	    <?php do_action( 'peggi_select_action_after_header_menu_area_html_open' ); ?>
	    
        <?php if($menu_area_in_grid) : ?>
            <div class="qodef-grid">
        <?php endif; ?>
	            
            <div class="qodef-vertical-align-containers">
	           <!--  <div class="qodef-position-left">
                    <div class="qodef-position-left-inner">
			            <div class="qodef-centered-widget-holder">
				            < ?php peggi_select_get_centered_header_widget_menu_area_left(); ?>
			            </div>
		            </div>
	            </div> -->
                <div class="qodef-position-center"><!--
                 --><div class="qodef-position-center-inner">
                        <?php peggi_select_get_main_menu(); ?>
                    </div>
                </div>
	            <!-- <div class="qodef-position-right">
                    <div class="qodef-position-right-inner">
			            <div class="qodef-centered-widget-holder">
			                < ?php peggi_select_get_centered_header_widget_menu_area_right(); ?>
			            </div>
		            </div>
	            </div> -->
            </div>
        <?php if($menu_area_in_grid) : ?>
            </div>
        <?php endif; ?>
    </div>
	
    <?php if($show_fixed_wrapper) { ?>
        </div>
	<?php } ?>
	
	<?php if($show_sticky) {
		peggi_select_get_sticky_header('centered', 'header/types/header-centered');
	} ?>
	
	<?php do_action('peggi_select_action_before_page_header_html_close'); ?>
</header>

<?php do_action('peggi_select_action_after_page_header'); ?>