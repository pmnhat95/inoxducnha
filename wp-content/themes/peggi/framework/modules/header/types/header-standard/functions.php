<?php

if ( ! function_exists( 'peggi_select_register_header_standard_type' ) ) {
	/**
	 * This function is used to register header type class for header factory file
	 */
	function peggi_select_register_header_standard_type( $header_types ) {
		$header_type = array(
			'header-standard' => 'PeggiSelectNamespace\Modules\Header\Types\HeaderStandard'
		);
		
		$header_types = array_merge( $header_types, $header_type );
		
		return $header_types;
	}
}

if ( ! function_exists( 'peggi_select_init_register_header_standard_type' ) ) {
	/**
	 * This function is used to wait header-function.php file to init header object and then to init hook registration function above
	 */
	function peggi_select_init_register_header_standard_type() {
		add_filter( 'peggi_select_filter_register_header_type_class', 'peggi_select_register_header_standard_type' );
	}
	
	add_action( 'peggi_select_action_before_header_function_init', 'peggi_select_init_register_header_standard_type' );
}