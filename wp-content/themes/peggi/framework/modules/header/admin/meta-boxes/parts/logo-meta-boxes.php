<?php

if ( ! function_exists( 'peggi_select_logo_meta_box_map' ) ) {
	function peggi_select_logo_meta_box_map() {
		
		$logo_meta_box = peggi_select_create_meta_box(
			array(
				'scope' => apply_filters( 'peggi_select_filter_set_scope_for_meta_boxes', array( 'page', 'post' ), 'logo_meta' ),
				'title' => esc_html__( 'Logo', 'peggi' ),
				'name'  => 'logo_meta'
			)
		);
		
		peggi_select_create_meta_box_field(
			array(
				'name'        => 'qodef_logo_image_meta',
				'type'        => 'image',
				'label'       => esc_html__( 'Logo Image - Default', 'peggi' ),
				'description' => esc_html__( 'Choose a default logo image to display ', 'peggi' ),
				'parent'      => $logo_meta_box
			)
		);
		
		peggi_select_create_meta_box_field(
			array(
				'name'        => 'qodef_logo_image_dark_meta',
				'type'        => 'image',
				'label'       => esc_html__( 'Logo Image - Dark', 'peggi' ),
				'description' => esc_html__( 'Choose a default logo image to display ', 'peggi' ),
				'parent'      => $logo_meta_box
			)
		);
		
		peggi_select_create_meta_box_field(
			array(
				'name'        => 'qodef_logo_image_light_meta',
				'type'        => 'image',
				'label'       => esc_html__( 'Logo Image - Light', 'peggi' ),
				'description' => esc_html__( 'Choose a default logo image to display ', 'peggi' ),
				'parent'      => $logo_meta_box
			)
		);
		
		peggi_select_create_meta_box_field(
			array(
				'name'        => 'qodef_logo_image_sticky_meta',
				'type'        => 'image',
				'label'       => esc_html__( 'Logo Image - Sticky', 'peggi' ),
				'description' => esc_html__( 'Choose a default logo image to display ', 'peggi' ),
				'parent'      => $logo_meta_box
			)
		);
		
		peggi_select_create_meta_box_field(
			array(
				'name'        => 'qodef_logo_image_mobile_meta',
				'type'        => 'image',
				'label'       => esc_html__( 'Logo Image - Mobile', 'peggi' ),
				'description' => esc_html__( 'Choose a default logo image to display ', 'peggi' ),
				'parent'      => $logo_meta_box
			)
		);
	}
	
	add_action( 'peggi_select_action_meta_boxes_map', 'peggi_select_logo_meta_box_map', 47 );
}