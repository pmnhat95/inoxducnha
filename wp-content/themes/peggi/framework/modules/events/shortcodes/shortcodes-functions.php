<?php

if ( ! function_exists( 'peggi_select_include_events_shortcodes' ) ) {
	function peggi_select_include_events_shortcodes() {
		foreach ( glob( SELECT_FRAMEWORK_MODULES_ROOT_DIR . '/events/shortcodes/*/load.php' ) as $shortcode_load ) {
			include_once $shortcode_load;
		}
	}
	
	if ( peggi_select_core_plugin_installed() ) {
		add_action( 'peggi_core_action_include_shortcodes_file', 'peggi_select_include_events_shortcodes' );
	}
}

if ( ! function_exists( 'peggi_select_set_product_list_icon_class_name_for_vc_shortcodes' ) ) {
	/**
	 * Function that set custom icon class name for event list shortcodes to set our icon for Visual Composer shortcodes panel
	 */
	function peggi_select_set_product_list_icon_class_name_for_vc_shortcodes( $shortcodes_icon_class_array ) {
		$shortcodes_icon_class_array[] = '.icon-wpb-events-list';
		
		return $shortcodes_icon_class_array;
	}
	
	if ( peggi_select_core_plugin_installed() ) {
		add_filter( 'peggi_core_filter_add_vc_shortcodes_custom_icon_class', 'peggi_select_set_product_list_icon_class_name_for_vc_shortcodes' );
	}
}