<?php

if ( ! function_exists( 'peggi_select_add_events_list_shortcodes' ) ) {
	function peggi_select_add_events_list_shortcodes( $shortcodes_class_name ) {
		$shortcodes = array(
			'PeggiCore\CPT\Shortcodes\EventsList\EventsList'
		);
		
		$shortcodes_class_name = array_merge( $shortcodes_class_name, $shortcodes );
		
		return $shortcodes_class_name;
	}
	
	add_filter( 'peggi_core_filter_add_vc_shortcode', 'peggi_select_add_events_list_shortcodes' );
}