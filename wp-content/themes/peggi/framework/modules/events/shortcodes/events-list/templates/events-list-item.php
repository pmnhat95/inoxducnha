<?php
$price_class = '';
if ( tribe_get_cost( null, true ) == 'Free' ) {
	$price_class = 'qodef-free';
}

$item_class = 'qodef-item-space';
?>
<div <?php post_class( $item_class ); ?>>
	<div class="qodef-events-list-item-holder" <?php echo peggi_select_get_inline_style($holder_styles); ?>>
		<?php if ( has_post_thumbnail() ) : ?>
			<div class="qodef-events-list-item-image-holder">
				<a href="<?php the_permalink(); ?>">
					<?php the_post_thumbnail( $image_size ); ?>
					
					<div class="qodef-events-list-item-date-holder">
						<div class="qodef-events-list-item-date-inner">
							<h5 class="qodef-events-list-item-date-day">
								<?php echo tribe_get_start_date( null, true, 'd' ); ?>
							</h5>
							
							<span class="qodef-events-list-item-date-month">
								<?php echo tribe_get_start_date( null, true, 'M' ); ?>
							</span>
						</div>
					</div>
				</a>
			</div>
		<?php endif; ?>
		<div class="qodef-events-list-item-content">
			<div class="qodef-events-list-item-title-holder">
				<h4 class="qodef-events-list-item-title">
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
				</h4>
			</div>
			<div class="qodef-events-list-item-info">
				<div class="qodef-events-list-item-date">
					<span class="qodef-events-item-info-icon">
						<?php echo peggi_select_icon_collections()->renderIcon( 'lnr-calendar-full', 'linear_icons' ); ?>
					</span>
					<?php echo tribe_get_start_date( null, true, 'M j, Y' ); ?>
				</div>
				<div class="qodef-events-list-item-time">
					<span class="qodef-events-item-info-icon">
						<?php echo peggi_select_icon_collections()->renderIcon( 'lnr-clock', 'linear_icons' ); ?>
					</span>
					<?php echo tribe_get_start_time(); ?> - <?php echo tribe_get_end_time(); ?>
				</div>
				<div class="qodef-events-list-item-location-holder">
					<span class="qodef-events-item-info-icon">
						<?php echo peggi_select_icon_collections()->renderIcon( 'lnr-map-marker', 'linear_icons' ); ?>
					</span>
					<span class="qpdef-events-list-item-location"><?php echo esc_html( tribe_get_address() ); ?></span>
				</div>
				<div class="qodef-events-list-item-price-holder">
					<span class="qodef-events-list-item-price <?php echo esc_html( $price_class ); ?>">
						<?php echo esc_html( tribe_get_cost( null, true ) ); ?>
					</span>
				</div>
			</div>
		</div>
	</div>
</div>