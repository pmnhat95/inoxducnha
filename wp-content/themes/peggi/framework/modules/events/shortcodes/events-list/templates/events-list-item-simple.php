<?php
$price_class = '';
if ( tribe_get_cost( null, true ) == 'Free' ) {
	$price_class = 'qodef-free';
}

$item_class = 'qodef-item-space';
?>
<div <?php post_class( $item_class ); ?>>
	<div class="qodef-events-list-item-holder" <?php echo peggi_select_get_inline_style($holder_styles); ?>>
		<div class="qodef-events-list-item-content">
			<div class="qodef-events-list-item-title-holder">
				<h5 class="qodef-events-list-item-title">
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
				</h5>
			</div>
			<div class="qodef-events-list-item-info">
				<div class="qodef-events-list-item-date">
					<?php echo tribe_events_event_schedule_details(); ?>
				</div>
			</div>
		</div>
	</div>
</div>