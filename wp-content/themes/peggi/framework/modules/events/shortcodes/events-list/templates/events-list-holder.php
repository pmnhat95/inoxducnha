<div class="qodef-events-list qodef-grid-list qodef-disable-bottom-space <?php echo esc_attr( $holder_classes ); ?>">
	<div class="qodef-el-wrapper qodef-outer-space">
		<?php
		if ( $query_result->have_posts() ):
			while ( $query_result->have_posts() ) : $query_result->the_post();
				peggi_select_get_module_template_part( 'templates/events-list-item', 'events/shortcodes/events-list', $type, $params );
			endwhile;
		else: ?>
			<p><?php esc_html_e( 'Sorry, no posts matched your criteria.', 'peggi' ); ?></p>
		<?php endif;
		
		wp_reset_postdata();
		?>
	</div>
</div>