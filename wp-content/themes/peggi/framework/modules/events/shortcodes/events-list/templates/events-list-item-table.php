<?php
$price_class = '';
if ( tribe_get_cost( null, true ) == 'Free' ) {
	$price_class = 'qodef-free';
}

?>
<div class="qodef-el-item-holder ">
	<div class="qodef-el-item-date-holder">
		<div class="qodef-el-item-date">
			<h2 class="qodef-el-item-date-day">
				<?php echo tribe_get_start_date( null, true, 'd' ); ?>
			</h2>
			<span class="qodef-el-item-date-day-title">
				<?php echo tribe_get_start_date( null, true, 'l' ); ?>
			</span>
		</div>
	</div>
	<div class="qodef-el-item-conntent-holder">
			<h5 class="qodef-el-item-title">
				<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
			</h5>
		<div class="qodef-el-item-date-and-time-holder">
			<div class="qodef-el-item-date">
				<span class="qodef-events-item-info-icon">
					<?php echo peggi_select_icon_collections()->renderIcon( 'lnr-calendar-full', 'linear_icons' ); ?>
				</span>
				<?php echo tribe_get_start_date( null, true, 'M j, Y' ); ?>
			</div>
			<div class="qodef-el-item-time">
				<span class="qodef-events-item-info-icon">
					<?php echo peggi_select_icon_collections()->renderIcon( 'lnr-clock', 'linear_icons' ); ?>
				</span>
				<?php echo tribe_get_start_time(); ?> - <?php echo tribe_get_end_time(); ?>
			</div>
		</div>
		<div class="qodef-el-item-location-and-price-holder">
			<div class="qodef-el-item-location-holder">
				<span class="qodef-events-item-info-icon">
					<?php echo peggi_select_icon_collections()->renderIcon( 'lnr-map-marker', 'linear_icons' ); ?>
				</span>
				<span class="qpdef-el-item-location"><?php echo esc_html( tribe_get_address() ); ?></span>
			</div>
			<div class="qodef-el-item-price-holder">
				<span class="qodef-el-item-price <?php echo esc_html( $price_class ); ?>">
					<?php echo esc_html( tribe_get_cost( null, true ) ); ?>
				</span>
			</div>
		</div>
	</div>
</div>