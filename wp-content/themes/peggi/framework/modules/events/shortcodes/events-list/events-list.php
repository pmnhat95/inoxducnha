<?php
namespace PeggiCore\CPT\Shortcodes\EventsList;

use PeggiCore\Lib;

class EventsList implements Lib\ShortcodeInterface {
	private $base;
	
	public function __construct() {
		$this->base = 'qodef_events_list';
		
		add_action( 'vc_before_init', array( $this, 'vcMap' ) );
	}
	
	public function getBase() {
		return $this->base;
	}
	
	public function vcMap() {
		vc_map(
			array(
				'name'     => esc_html__( 'Events List', 'peggi' ),
				'base'     => $this->getBase(),
				'category' => esc_html__( 'by PEGGI', 'peggi' ),
				'icon'     => 'icon-wpb-events-list extended-custom-icon',
				'params'   => array(
					array(
						'type'        => 'dropdown',
						'param_name'  => 'type',
						'heading'     => esc_html__( 'Type', 'peggi' ),
						'value'       => array(
							esc_html__( 'List', 'peggi' )   => 'list',
							esc_html__( 'Simple', 'peggi' ) => 'simple',
							esc_html__( 'Spilt', 'peggi' )  => 'split',
							esc_html__( 'Table', 'peggi' )  => 'table'
						),
						'save_always' => true
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'split_skin',
						'heading'    => esc_html__( 'Skin', 'peggi' ),
						'value'       => array(
							esc_html__( 'Default', 'peggi' )  => '',
							esc_html__( 'Dark', 'peggi' )     => 'dark',
						),
						'dependency' => array( 'element' => 'type', 'value' => array( 'split' ) ),
						'save_always' => true,
					),
					array(
						'type'       => 'colorpicker',
						'param_name' => 'box_color',
						'heading'    => esc_html__( 'Box Color', 'peggi' ),
						'dependency' => array( 'element' => 'type', 'value' => array( 'list', 'simple' ) ),
					),
					array(
						'type'        => 'textfield',
						'param_name'  => 'number_of_events',
						'value'       => '-1',
						'heading'     => esc_html__( 'Number of Events', 'peggi' ),
						'description' => esc_html__( 'Enter -1 to show all', 'peggi' )
					),
					array(
						'type'       => 'dropdown',
						'param_name' => 'number_of_columns',
						'heading'    => esc_html__( 'Number of Columns', 'peggi' ),
						'value'      => array_flip( peggi_select_get_number_of_columns_array( true ) ),
						'dependency'  => array( 'element' => 'type', 'value' => array( 'list', 'simple' ) )
					),
					array(
						'type'        => 'dropdown',
						'param_name'  => 'space_between_items',
						'heading'     => esc_html__( 'Space Between Items', 'peggi' ),
						'value'       => array_flip( peggi_select_get_space_between_items_array() ),
						'dependency'  => array( 'element' => 'type', 'value' => array( 'list', 'simple', 'split' ) ),
						'save_always' => true
					),
					array(
						'type'        => 'dropdown',
						'param_name'  => 'image_size',
						'heading'     => esc_html__( 'Image Proportions', 'peggi' ),
						'value'       => array(
							esc_html__( 'Original', 'peggi' )  => 'full',
							esc_html__( 'Square', 'peggi' )    => 'square',
							esc_html__( 'Landscape', 'peggi' ) => 'landscape',
							esc_html__( 'Portrait', 'peggi' )  => 'portrait'
						),
						'dependency'  => array( 'element' => 'type', 'value' => array( 'list', 'simple', 'split' ) ),
						'save_always' => true
					),
					array(
						'type'        => 'dropdown',
						'param_name'  => 'orderby',
						'heading'     => esc_html__( 'Order By', 'peggi' ),
						'value'       => array_flip( peggi_select_get_query_order_by_array() ),
						'save_always' => true
					),
					array(
						'type'        => 'dropdown',
						'param_name'  => 'order',
						'heading'     => esc_html__( 'Order', 'peggi' ),
						'value'       => array_flip( peggi_select_get_query_order_array() ),
						'save_always' => true
					),
					array(
						'type'        => 'textfield',
						'param_name'  => 'category',
						'heading'     => esc_html__( 'Events Category', 'peggi' ),
						'description' => esc_html__( 'Enter one category slug (leave empty for showing all categories)', 'peggi' ),
						'dependency'  => array( 'element' => 'type', 'value' => array( 'list', 'simple', 'split' ) )
					),
					array(
						'type'        => 'textfield',
						'param_name'  => 'selected_events',
						'heading'     => esc_html__( 'Show Only Events with Listed IDs', 'peggi' ),
						'description' => esc_html__( 'Delimit ID numbers by comma (leave empty for all)', 'peggi' )
					)
				)
			)
		);
	}
	
	public function render( $atts, $content = null ) {
		$args   = array(
			'type'                => 'list',
			'split_skin'          => '',
			'box_color'           => '',
			'hover_box_color'     => '',
			'number_of_events'    => '-1',
			'number_of_columns'   => 'two',
			'space_between_items' => 'normal',
			'image_size'          => '',
			'orderby'             => 'title',
			'order'               => 'ASC',
			'category'            => '',
			'selected_events'     => ''
		);
		$params = shortcode_atts( $args, $atts );
		
		$queryArray             = $this->generateQueryArray( $params );
		$query_result           = new \WP_Query( $queryArray );
		$params['query_result'] = $query_result;
		
		$params['type']         = ! empty( $params['type'] ) ? $params['type'] : $args['type'];
		
		$params['holder_classes']       = $this->getHolderClasses( $params, $args );
		$params['holder_styles']        = $this->getHolderStyles( $params );
		$params['image_size']           = $this->getImageSize( $params );
		
		ob_start();
		
		peggi_select_get_module_template_part( 'templates/events-list-holder', 'events/shortcodes/events-list', '', $params );
		
		$html = ob_get_contents();
		
		ob_end_clean();
		
		return $html;
	}
	
	public function generateQueryArray( $params ) {
		$queryArray = array(
			'post_status'    => 'publish',
			'post_type'      => 'tribe_events',
			'orderby'        => $params['orderby'],
			'order'          => $params['order'],
			'posts_per_page' => $params['number_of_events']
		);
		
		if ( ! empty( $params['category'] ) ) {
			$queryArray['tribe_events_cat'] = $params['category'];
		}
		
		$projectIds = null;
		if ( ! empty( $params['selected_events'] ) ) {
			$projectIds             = explode( ',', $params['selected_events'] );
			$queryArray['post__in'] = $projectIds;
		}
		
		if ( ! empty( $params['next_page'] ) ) {
			$queryArray['paged'] = $params['next_page'];
		} else {
			$queryArray['paged'] = 1;
		}
		
		return $queryArray;
	}
	
	public function getHolderClasses( $params, $args ) {
		$holderClasses = array();
		
		$holderClasses[] = ! empty( $params['type'] ) ? 'qodef-el-' . $params['type'] : 'qodef-el-' . $args['type'];
		$holderClasses[] = ! empty( $params['number_of_columns'] ) ? 'qodef-' . $params['number_of_columns'] . '-columns' : 'qodef-' . $args['number_of_columns'] . '-columns';
		$holderClasses[] = ! empty( $params['space_between_items'] ) ? 'qodef-' . $params['space_between_items'] . '-space' : 'qodef-' . $args['space_between_items'] . '-space';
		$holderClasses[] = ! empty( $params['split_skin'] ) ? 'qodef-skin-' . $params['split_skin'] : 'qodef-skin-' . $args['split_skin'];
		
		return implode( ' ', $holderClasses );
	}
	
	private function getHolderStyles( $params ) {
		$styles = array();
		
		if ( ! empty( $params['box_color'] ) ) {
			$styles[] = 'background-color: ' . $params['box_color'];
		}
		
		return implode( ';', $styles );
	}
	
	private function getImageSize( $params ) {
		if ( ! empty( $params['image_size'] ) ) {
			$image_size = $params['image_size'];
			
			switch ( $image_size ) {
				case 'landscape':
					$thumb_size = 'peggi_select_image_landscape';
					break;
				case 'portrait':
					$thumb_size = 'peggi_select_image_portrait';
					break;
				case 'square':
					$thumb_size = 'peggi_select_image_square';
					break;
				case 'full':
					$thumb_size = 'full';
					break;
				case 'custom':
					$thumb_size = 'custom';
					break;
				default:
					$thumb_size = 'full';
					break;
			}
			
			return $thumb_size;
		}
	}
}