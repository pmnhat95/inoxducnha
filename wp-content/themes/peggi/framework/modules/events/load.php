<?php

if ( peggi_select_the_events_calendar_installed() ) {
	include_once SELECT_FRAMEWORK_MODULES_ROOT_DIR . '/events/events-functions.php';
	include_once SELECT_FRAMEWORK_MODULES_ROOT_DIR . '/events/shortcodes/shortcodes-functions.php';
}