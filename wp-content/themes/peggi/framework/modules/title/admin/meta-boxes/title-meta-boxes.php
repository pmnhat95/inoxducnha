<?php

if ( ! function_exists( 'peggi_select_get_title_types_meta_boxes' ) ) {
	function peggi_select_get_title_types_meta_boxes() {
		$title_type_options = apply_filters( 'peggi_select_filter_title_type_meta_boxes', $title_type_options = array( '' => esc_html__( 'Default', 'peggi' ) ) );
		
		return $title_type_options;
	}
}

foreach ( glob( SELECT_FRAMEWORK_MODULES_ROOT_DIR . '/title/types/*/admin/meta-boxes/*.php' ) as $meta_box_load ) {
	include_once $meta_box_load;
}

if ( ! function_exists( 'peggi_select_map_title_meta' ) ) {
	function peggi_select_map_title_meta() {
		$title_type_meta_boxes = peggi_select_get_title_types_meta_boxes();
		
		$title_meta_box = peggi_select_create_meta_box(
			array(
				'scope' => apply_filters( 'peggi_select_filter_set_scope_for_meta_boxes', array( 'page', 'post' ), 'title_meta' ),
				'title' => esc_html__( 'Title', 'peggi' ),
				'name'  => 'title_meta'
			)
		);
		
		peggi_select_create_meta_box_field(
			array(
				'name'          => 'qodef_show_title_area_meta',
				'type'          => 'select',
				'default_value' => '',
				'label'         => esc_html__( 'Show Title Area', 'peggi' ),
				'description'   => esc_html__( 'Disabling this option will turn off page title area', 'peggi' ),
				'parent'        => $title_meta_box,
				'options'       => peggi_select_get_yes_no_select_array()
			)
		);
		
			$show_title_area_meta_container = peggi_select_add_admin_container(
				array(
					'parent'          => $title_meta_box,
					'name'            => 'qodef_show_title_area_meta_container',
					'dependency' => array(
						'hide' => array(
							'qodef_show_title_area_meta' => 'no'
						)
					)
				)
			);
		
				peggi_select_create_meta_box_field(
					array(
						'name'          => 'qodef_title_area_type_meta',
						'type'          => 'select',
						'default_value' => '',
						'label'         => esc_html__( 'Title Area Type', 'peggi' ),
						'description'   => esc_html__( 'Choose title type', 'peggi' ),
						'parent'        => $show_title_area_meta_container,
						'options'       => $title_type_meta_boxes
					)
				);
		
				peggi_select_create_meta_box_field(
					array(
						'name'          => 'qodef_title_area_in_grid_meta',
						'type'          => 'select',
						'default_value' => '',
						'label'         => esc_html__( 'Title Area In Grid', 'peggi' ),
						'description'   => esc_html__( 'Set title area content to be in grid', 'peggi' ),
						'options'       => peggi_select_get_yes_no_select_array(),
						'parent'        => $show_title_area_meta_container
					)
				);
		
				peggi_select_create_meta_box_field(
					array(
						'name'        => 'qodef_title_area_height_meta',
						'type'        => 'text',
						'label'       => esc_html__( 'Height', 'peggi' ),
						'description' => esc_html__( 'Set a height for Title Area', 'peggi' ),
						'parent'      => $show_title_area_meta_container,
						'args'        => array(
							'col_width' => 2,
							'suffix'    => 'px'
						)
					)
				);
				
				peggi_select_create_meta_box_field(
					array(
						'name'        => 'qodef_title_area_background_color_meta',
						'type'        => 'color',
						'label'       => esc_html__( 'Background Color', 'peggi' ),
						'description' => esc_html__( 'Choose a background color for title area', 'peggi' ),
						'parent'      => $show_title_area_meta_container
					)
				);
		
				peggi_select_create_meta_box_field(
					array(
						'name'        => 'qodef_title_area_background_image_meta',
						'type'        => 'image',
						'label'       => esc_html__( 'Background Image', 'peggi' ),
						'description' => esc_html__( 'Choose an Image for title area', 'peggi' ),
						'parent'      => $show_title_area_meta_container
					)
				);
		
				peggi_select_create_meta_box_field(
					array(
						'name'          => 'qodef_title_area_background_image_behavior_meta',
						'type'          => 'select',
						'default_value' => '',
						'label'         => esc_html__( 'Background Image Behavior', 'peggi' ),
						'description'   => esc_html__( 'Choose title area background image behavior', 'peggi' ),
						'parent'        => $show_title_area_meta_container,
						'options'       => array(
							''                    => esc_html__( 'Default', 'peggi' ),
							'hide'                => esc_html__( 'Hide Image', 'peggi' ),
							'responsive'          => esc_html__( 'Enable Responsive Image', 'peggi' ),
							'responsive-disabled' => esc_html__( 'Disable Responsive Image', 'peggi' ),
							'parallax'            => esc_html__( 'Enable Parallax Image', 'peggi' ),
							'parallax-zoom-out'   => esc_html__( 'Enable Parallax With Zoom Out Image', 'peggi' ),
							'parallax-disabled'   => esc_html__( 'Disable Parallax Image', 'peggi' )
						)
					)
				);
				
				peggi_select_create_meta_box_field(
					array(
						'name'          => 'qodef_title_area_vertical_alignment_meta',
						'type'          => 'select',
						'default_value' => '',
						'label'         => esc_html__( 'Vertical Alignment', 'peggi' ),
						'description'   => esc_html__( 'Specify title area content vertical alignment', 'peggi' ),
						'parent'        => $show_title_area_meta_container,
						'options'       => array(
							''              => esc_html__( 'Default', 'peggi' ),
							'header-bottom' => esc_html__( 'From Bottom of Header', 'peggi' ),
							'window-top'    => esc_html__( 'From Window Top', 'peggi' )
						)
					)
				);
				
				peggi_select_create_meta_box_field(
					array(
						'name'          => 'qodef_title_area_title_tag_meta',
						'type'          => 'select',
						'default_value' => '',
						'label'         => esc_html__( 'Title Tag', 'peggi' ),
						'options'       => peggi_select_get_title_tag( true ),
						'parent'        => $show_title_area_meta_container
					)
				);
				
				peggi_select_create_meta_box_field(
					array(
						'name'        => 'qodef_title_text_color_meta',
						'type'        => 'color',
						'label'       => esc_html__( 'Title Color', 'peggi' ),
						'description' => esc_html__( 'Choose a color for title text', 'peggi' ),
						'parent'      => $show_title_area_meta_container
					)
				);
				
				peggi_select_create_meta_box_field(
					array(
						'name'          => 'qodef_title_area_subtitle_meta',
						'type'          => 'text',
						'default_value' => '',
						'label'         => esc_html__( 'Subtitle Text', 'peggi' ),
						'description'   => esc_html__( 'Enter your subtitle text', 'peggi' ),
						'parent'        => $show_title_area_meta_container,
						'args'          => array(
							'col_width' => 6
						)
					)
				);
		
				peggi_select_create_meta_box_field(
					array(
						'name'          => 'qodef_title_area_subtitle_tag_meta',
						'type'          => 'select',
						'default_value' => '',
						'label'         => esc_html__( 'Subtitle Tag', 'peggi' ),
						'options'       => peggi_select_get_title_tag( true, array( 'p' => 'p' ) ),
						'parent'        => $show_title_area_meta_container
					)
				);
				
				peggi_select_create_meta_box_field(
					array(
						'name'        => 'qodef_subtitle_color_meta',
						'type'        => 'color',
						'label'       => esc_html__( 'Subtitle Color', 'peggi' ),
						'description' => esc_html__( 'Choose a color for subtitle text', 'peggi' ),
						'parent'      => $show_title_area_meta_container
					)
				);
		
		/***************** Additional Title Area Layout - start *****************/
		
		do_action( 'peggi_select_action_additional_title_area_meta_boxes', $show_title_area_meta_container );
		
		/***************** Additional Title Area Layout - end *****************/
		
	}
	
	add_action( 'peggi_select_action_meta_boxes_map', 'peggi_select_map_title_meta', 60 );
}