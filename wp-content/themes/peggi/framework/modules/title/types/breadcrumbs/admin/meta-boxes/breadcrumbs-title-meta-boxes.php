<?php

if ( ! function_exists( 'peggi_select_breadcrumbs_title_type_options_meta_boxes' ) ) {
	function peggi_select_breadcrumbs_title_type_options_meta_boxes( $show_title_area_meta_container ) {
		
		peggi_select_create_meta_box_field(
			array(
				'name'        => 'qodef_breadcrumbs_color_meta',
				'type'        => 'color',
				'label'       => esc_html__( 'Breadcrumbs Color', 'peggi' ),
				'description' => esc_html__( 'Choose a color for breadcrumbs text', 'peggi' ),
				'parent'      => $show_title_area_meta_container
			)
		);
	}
	
	add_action( 'peggi_select_action_additional_title_area_meta_boxes', 'peggi_select_breadcrumbs_title_type_options_meta_boxes' );
}