<?php

if ( ! function_exists( 'peggi_select_boxed_title_type_options_meta_boxes' ) ) {
	function peggi_select_boxed_title_type_options_meta_boxes( $show_title_area_meta_container ) {
		
		peggi_select_create_meta_box_field(
			array(
				'name'        => 'qodef_subtitle_side_padding_meta',
				'type'        => 'text',
				'label'       => esc_html__( 'Subtitle Side Padding', 'peggi' ),
				'description' => esc_html__( 'Set left/right padding for subtitle area', 'peggi' ),
				'parent'      => $show_title_area_meta_container,
				'args'        => array(
					'col_width' => 2,
					'suffix'    => 'px or %'
				)
			)
		);
	}
	
	add_action( 'peggi_select_action_additional_title_area_meta_boxes', 'peggi_select_boxed_title_type_options_meta_boxes', 5 );
}