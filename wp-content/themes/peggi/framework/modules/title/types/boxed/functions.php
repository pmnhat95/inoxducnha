<?php

if ( ! function_exists( 'peggi_select_set_title_boxed_type_for_options' ) ) {
	/**
	 * This function set centered title type value for title options map and meta boxes
	 */
	function peggi_select_set_title_boxed_type_for_options( $type ) {
		$type['boxed'] = esc_html__( 'Boxed', 'peggi' );
		
		return $type;
	}
	
	add_filter( 'peggi_select_filter_title_type_global_option', 'peggi_select_set_title_boxed_type_for_options' );
	add_filter( 'peggi_select_filter_title_type_meta_boxes', 'peggi_select_set_title_boxed_type_for_options' );
}