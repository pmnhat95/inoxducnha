<?php

if (!function_exists('peggi_select_sidearea_options_map')) {
    function peggi_select_sidearea_options_map() {

        peggi_select_add_admin_page(
            array(
                'slug'  => '_side_area_page',
                'title' => esc_html__('Side Area', 'peggi'),
                'icon'  => 'fa fa-indent'
            )
        );

        $side_area_panel = peggi_select_add_admin_panel(
            array(
                'title' => esc_html__('Side Area', 'peggi'),
                'name'  => 'side_area',
                'page'  => '_side_area_page'
            )
        );

        peggi_select_add_admin_field(
            array(
                'parent'        => $side_area_panel,
                'type'          => 'select',
                'name'          => 'side_area_type',
                'default_value' => 'side-menu-slide-from-right',
                'label'         => esc_html__('Side Area Type', 'peggi'),
                'description'   => esc_html__('Choose a type of Side Area', 'peggi'),
                'options'       => array(
                    'side-menu-slide-from-right'       => esc_html__('Slide from Right Over Content', 'peggi'),
                    'side-menu-slide-with-content'     => esc_html__('Slide from Right With Content', 'peggi'),
                    'side-area-uncovered-from-content' => esc_html__('Side Area Uncovered from Content', 'peggi'),
                ),
            )
        );

        peggi_select_add_admin_field(
            array(
                'parent'        => $side_area_panel,
                'type'          => 'text',
                'name'          => 'side_area_width',
                'default_value' => '',
                'label'         => esc_html__('Side Area Width', 'peggi'),
                'description'   => esc_html__('Enter a width for Side Area (px or %). Default width: 405px.', 'peggi'),
                'args'          => array(
                    'col_width' => 3,
                )
            )
        );

        $side_area_width_container = peggi_select_add_admin_container(
            array(
                'parent'     => $side_area_panel,
                'name'       => 'side_area_width_container',
                'dependency' => array(
                    'show' => array(
                        'side_area_type' => 'side-menu-slide-from-right',
                    )
                )
            )
        );

        peggi_select_add_admin_field(
            array(
                'parent'        => $side_area_width_container,
                'type'          => 'color',
                'name'          => 'side_area_content_overlay_color',
                'default_value' => '',
                'label'         => esc_html__('Content Overlay Background Color', 'peggi'),
                'description'   => esc_html__('Choose a background color for a content overlay', 'peggi'),
            )
        );

        peggi_select_add_admin_field(
            array(
                'parent'        => $side_area_width_container,
                'type'          => 'text',
                'name'          => 'side_area_content_overlay_opacity',
                'default_value' => '',
                'label'         => esc_html__('Content Overlay Background Transparency', 'peggi'),
                'description'   => esc_html__('Choose a transparency for the content overlay background color (0 = fully transparent, 1 = opaque)', 'peggi'),
                'args'          => array(
                    'col_width' => 3
                )
            )
        );

        peggi_select_add_admin_field(
            array(
                'parent'        => $side_area_panel,
                'type'          => 'select',
                'name'          => 'side_area_icon_source',
                'default_value' => 'icon_pack',
                'label'         => esc_html__('Select Side Area Icon Source', 'peggi'),
                'description'   => esc_html__('Choose whether you would like to use icons from an icon pack or SVG icons', 'peggi'),
                'options'       => peggi_select_get_icon_sources_array()
            )
        );

        $side_area_icon_pack_container = peggi_select_add_admin_container(
            array(
                'parent'     => $side_area_panel,
                'name'       => 'side_area_icon_pack_container',
                'dependency' => array(
                    'show' => array(
                        'side_area_icon_source' => 'icon_pack'
                    )
                )
            )
        );

        peggi_select_add_admin_field(
            array(
                'parent'        => $side_area_icon_pack_container,
                'type'          => 'select',
                'name'          => 'side_area_icon_pack',
                'default_value' => 'font_elegant',
                'label'         => esc_html__('Side Area Icon Pack', 'peggi'),
                'description'   => esc_html__('Choose icon pack for Side Area icon', 'peggi'),
                'options'       => peggi_select_icon_collections()->getIconCollectionsExclude(array('linea_icons', 'dripicons', 'simple_line_icons'))
            )
        );

        $side_area_svg_icons_container = peggi_select_add_admin_container(
            array(
                'parent'     => $side_area_panel,
                'name'       => 'side_area_svg_icons_container',
                'dependency' => array(
                    'show' => array(
                        'side_area_icon_source' => 'svg_path'
                    )
                )
            )
        );

        peggi_select_add_admin_field(
            array(
                'parent'      => $side_area_svg_icons_container,
                'type'        => 'textarea',
                'name'        => 'side_area_icon_svg_path',
                'label'       => esc_html__('Side Area Icon SVG Path', 'peggi'),
                'description' => esc_html__('Enter your Side Area icon SVG path here. Please remove version and id attributes from your SVG path because of HTML validation', 'peggi'),
            )
        );

        peggi_select_add_admin_field(
            array(
                'parent'      => $side_area_svg_icons_container,
                'type'        => 'textarea',
                'name'        => 'side_area_close_icon_svg_path',
                'label'       => esc_html__('Side Area Close Icon SVG Path', 'peggi'),
                'description' => esc_html__('Enter your Side Area close icon SVG path here. Please remove version and id attributes from your SVG path because of HTML validation', 'peggi'),
            )
        );

        $side_area_icon_style_group = peggi_select_add_admin_group(
            array(
                'parent'      => $side_area_panel,
                'name'        => 'side_area_icon_style_group',
                'title'       => esc_html__('Side Area Icon Style', 'peggi'),
                'description' => esc_html__('Define styles for Side Area icon', 'peggi')
            )
        );

        $side_area_icon_style_row1 = peggi_select_add_admin_row(
            array(
                'parent' => $side_area_icon_style_group,
                'name'   => 'side_area_icon_style_row1'
            )
        );

        peggi_select_add_admin_field(
            array(
                'parent' => $side_area_icon_style_row1,
                'type'   => 'colorsimple',
                'name'   => 'side_area_icon_color',
                'label'  => esc_html__('Color', 'peggi')
            )
        );

        peggi_select_add_admin_field(
            array(
                'parent' => $side_area_icon_style_row1,
                'type'   => 'colorsimple',
                'name'   => 'side_area_icon_hover_color',
                'label'  => esc_html__('Hover Color', 'peggi')
            )
        );

        $side_area_icon_style_row2 = peggi_select_add_admin_row(
            array(
                'parent' => $side_area_icon_style_group,
                'name'   => 'side_area_icon_style_row2',
                'next'   => true
            )
        );

        peggi_select_add_admin_field(
            array(
                'parent' => $side_area_icon_style_row2,
                'type'   => 'colorsimple',
                'name'   => 'side_area_close_icon_color',
                'label'  => esc_html__('Close Icon Color', 'peggi')
            )
        );

        peggi_select_add_admin_field(
            array(
                'parent' => $side_area_icon_style_row2,
                'type'   => 'colorsimple',
                'name'   => 'side_area_close_icon_hover_color',
                'label'  => esc_html__('Close Icon Hover Color', 'peggi')
            )
        );

        peggi_select_add_admin_field(
            array(
                'parent'      => $side_area_panel,
                'type'        => 'color',
                'name'        => 'side_area_background_color',
                'label'       => esc_html__('Background Color', 'peggi'),
                'description' => esc_html__('Choose a background color for Side Area', 'peggi')
            )
        );

        peggi_select_add_admin_field(
            array(
                'parent'      => $side_area_panel,
                'type'        => 'text',
                'name'        => 'side_area_padding',
                'label'       => esc_html__('Padding', 'peggi'),
                'description' => esc_html__('Define padding for Side Area in format top right bottom left', 'peggi'),
                'args'        => array(
                    'col_width' => 3
                )
            )
        );

        peggi_select_add_admin_field(
            array(
                'parent'        => $side_area_panel,
                'type'          => 'selectblank',
                'name'          => 'side_area_aligment',
                'default_value' => '',
                'label'         => esc_html__('Text Alignment', 'peggi'),
                'description'   => esc_html__('Choose text alignment for side area', 'peggi'),
                'options'       => array(
                    ''       => esc_html__('Default', 'peggi'),
                    'left'   => esc_html__('Left', 'peggi'),
                    'center' => esc_html__('Center', 'peggi'),
                    'right'  => esc_html__('Right', 'peggi')
                )
            )
        );
    }

    add_action('peggi_select_action_options_map', 'peggi_select_sidearea_options_map', 16);
}