<?php

if ( ! function_exists( 'peggi_select_register_sidearea_opener_widget' ) ) {
	/**
	 * Function that register sidearea opener widget
	 */
	function peggi_select_register_sidearea_opener_widget( $widgets ) {
		$widgets[] = 'PeggiSelectClassSideAreaOpener';
		
		return $widgets;
	}
	
	add_filter( 'peggi_select_filter_register_widgets', 'peggi_select_register_sidearea_opener_widget' );
}