<?php

class PeggiSelectClassSideAreaOpener extends PeggiSelectClassWidget {
	public function __construct() {
		parent::__construct(
			'qodef_side_area_opener',
			esc_html__( 'Peggi Side Area Opener', 'peggi' ),
			array( 'description' => esc_html__( 'Display a "hamburger" icon that opens the side area', 'peggi' ) )
		);
		
		$this->setParams();
	}
	
	protected function setParams() {
		$this->params = array(
			array(
				'type'        => 'colorpicker',
				'name'        => 'icon_color',
				'title'       => esc_html__( 'Side Area Opener Color', 'peggi' ),
				'description' => esc_html__( 'Define color for side area opener', 'peggi' )
			),
			array(
				'type'        => 'colorpicker',
				'name'        => 'icon_hover_color',
				'title'       => esc_html__( 'Side Area Opener Hover Color', 'peggi' ),
				'description' => esc_html__( 'Define hover color for side area opener', 'peggi' )
			),
			array(
				'type'        => 'textfield',
				'name'        => 'widget_margin',
				'title'       => esc_html__( 'Side Area Opener Margin', 'peggi' ),
				'description' => esc_html__( 'Insert margin in format: top right bottom left (e.g. 10px 5px 10px 5px)', 'peggi' )
			),
			array(
				'type'  => 'textfield',
				'name'  => 'widget_title',
				'title' => esc_html__( 'Side Area Opener Title', 'peggi' )
			)
		);
	}
	
	public function widget( $args, $instance ) {
		$classes = array(
			'qodef-side-menu-button-opener',
			'qodef-icon-has-hover'
		);
		
		$classes[] = peggi_select_get_icon_sources_class( 'side_area', 'qodef-side-menu-button-opener' );

		$styles = array();
		if ( ! empty( $instance['icon_color'] ) ) {
			$styles[] = 'color: ' . $instance['icon_color'] . ';';
		}
		if ( ! empty( $instance['widget_margin'] ) ) {
			$styles[] = 'margin: ' . $instance['widget_margin'];
		}
		?>
		
		<a <?php peggi_select_class_attribute( $classes ); ?> <?php echo peggi_select_get_inline_attr( $instance['icon_hover_color'], 'data-hover-color' ); ?> href="javascript:void(0)" <?php peggi_select_inline_style( $styles ); ?>>
			<?php if ( ! empty( $instance['widget_title'] ) ) { ?>
				<h5 class="qodef-side-menu-title"><?php echo esc_html( $instance['widget_title'] ); ?></h5>
			<?php } ?>
			<span class="qodef-side-menu-icon">
				<?php echo peggi_select_get_icon_sources_html( 'side_area' ); ?>
            </span>
		</a>
	<?php }
}