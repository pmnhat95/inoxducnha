<li class="qodef-bl-item qodef-item-space">
	<div class="qodef-bli-inner">
		<?php if (( $post_info_image == 'yes' )) {
			peggi_select_get_module_template_part( 'templates/parts/media', 'blog', '', $params );
		} ?>
		<?php if ($post_info_section == 'yes') { ?>
			<div class="qodef-bli-info-top">
				<?php
					if ($post_info_category == 'yes' ) {
						peggi_select_get_module_template_part( 'templates/parts/post-info/category', 'blog', '', $params );
					}
				?>
			</div>
		<?php } ?>
        <div class="qodef-bli-content" <?php echo peggi_select_get_inline_style($holder_styles); ?>>
	        <?php peggi_select_get_module_template_part( 'templates/parts/title', 'blog', '', $params ); ?>
	
	        <?php if ($post_info_section == 'yes') { ?>
		        <div class="qodef-bli-info-bottom">
			        <?php
				        if ( $post_info_author == 'yes' ) {
					        peggi_select_get_module_template_part( 'templates/parts/post-info/author', 'blog', '', $params );
				        }
			        ?>
		        </div>
	        <?php } ?>
	        
        </div>
	</div>
</li>