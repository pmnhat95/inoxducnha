<?php

if ( ! function_exists( 'peggi_select_include_blog_shortcodes' ) ) {
	function peggi_select_include_blog_shortcodes() {
		foreach ( glob( SELECT_FRAMEWORK_MODULES_ROOT_DIR . '/blog/shortcodes/*/load.php' ) as $shortcode_load ) {
			include_once $shortcode_load;
		}
	}
	
	if ( peggi_select_core_plugin_installed() ) {
		add_action( 'peggi_core_action_include_shortcodes_file', 'peggi_select_include_blog_shortcodes' );
	}
}
