<?php

peggi_select_get_single_post_format_html( $blog_single_type );

do_action( 'peggi_select_action_after_article_content' );

peggi_select_get_module_template_part( 'templates/parts/single/author-info', 'blog' );

peggi_select_get_module_template_part( 'templates/parts/single/single-navigation', 'blog' );

peggi_select_get_module_template_part( 'templates/parts/single/comments', 'blog' );

peggi_select_get_module_template_part( 'templates/parts/single/related-posts', 'blog', '', $single_info_params );