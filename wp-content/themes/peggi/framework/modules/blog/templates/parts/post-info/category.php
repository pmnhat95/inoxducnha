<div class="qodef-post-info-category">
    <?php if ( isset( $without_space_mark ) && $without_space_mark ) {
	    the_category(' ');
    } else {
	    the_category(', ');
    } ?>
</div>