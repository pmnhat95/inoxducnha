<?php
$post_format = isset( $post_format ) ? $post_format : '';

switch ( $post_format ) {
	case 'video':
		peggi_select_get_module_template_part('templates/parts/image', 'blog', '', $params);
		echo peggi_select_icon_collections()->renderIcon( 'arrow_triangle-right', 'font_elegant', array( 'icon_attributes' => array( 'class' => 'qodef-post-image-icon' ) ) );
		break;
	case 'audio':
		peggi_select_get_module_template_part('templates/parts/image', 'blog', '', $params);
		echo peggi_select_icon_collections()->renderIcon( 'icon_headphones', 'font_elegant', array( 'icon_attributes' => array( 'class' => 'qodef-post-image-icon' ) ) );
		break;
	default :
		peggi_select_get_module_template_part('templates/parts/media', 'blog', $post_format, $params);
		break;
}