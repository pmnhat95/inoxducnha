<?php
$post_classes[] = 'qodef-item-space';
?>
<article id="post-<?php the_ID(); ?>" <?php post_class($post_classes); ?>>
    <div class="qodef-post-content">
        <div class="qodef-post-heading">
	        <?php peggi_select_get_module_template_part('templates/parts/media', 'blog', 'mark', array_merge( $part_params,array( 'post_format' => $post_format ) ) ); ?>
	
	        <div class="qodef-post-info-top">
		        <?php peggi_select_get_module_template_part('templates/parts/post-info/category', 'blog', '', $part_params); ?>
	        </div>
        </div>
        <div class="qodef-post-text">
            <div class="qodef-post-text-inner">
                <div class="qodef-post-text-main">
                    <?php peggi_select_get_module_template_part('templates/parts/title', 'blog', '', $part_params); ?>
                </div>
                <div class="qodef-post-info-bottom clearfix">
                    <div class="qodef-post-info-bottom-left">
                        <?php peggi_select_get_module_template_part('templates/parts/post-info/author', 'blog', '', $part_params); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</article>