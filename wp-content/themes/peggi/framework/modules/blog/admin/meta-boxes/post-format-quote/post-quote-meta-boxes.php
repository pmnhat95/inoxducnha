<?php

if ( ! function_exists( 'peggi_select_map_post_quote_meta' ) ) {
	function peggi_select_map_post_quote_meta() {
		$quote_post_format_meta_box = peggi_select_create_meta_box(
			array(
				'scope' => array( 'post' ),
				'title' => esc_html__( 'Quote Post Format', 'peggi' ),
				'name'  => 'post_format_quote_meta'
			)
		);
		
		peggi_select_create_meta_box_field(
			array(
				'name'        => 'qodef_post_quote_text_meta',
				'type'        => 'text',
				'label'       => esc_html__( 'Quote Text', 'peggi' ),
				'description' => esc_html__( 'Enter Quote text', 'peggi' ),
				'parent'      => $quote_post_format_meta_box
			)
		);
		
		peggi_select_create_meta_box_field(
			array(
				'name'        => 'qodef_post_quote_author_meta',
				'type'        => 'text',
				'label'       => esc_html__( 'Quote Author', 'peggi' ),
				'description' => esc_html__( 'Enter Quote author', 'peggi' ),
				'parent'      => $quote_post_format_meta_box
			)
		);
	}
	
	add_action( 'peggi_select_action_meta_boxes_map', 'peggi_select_map_post_quote_meta', 25 );
}