<?php

if ( ! function_exists( 'peggi_select_map_post_video_meta' ) ) {
	function peggi_select_map_post_video_meta() {
		$video_post_format_meta_box = peggi_select_create_meta_box(
			array(
				'scope' => array( 'post' ),
				'title' => esc_html__( 'Video Post Format', 'peggi' ),
				'name'  => 'post_format_video_meta'
			)
		);
		
		peggi_select_create_meta_box_field(
			array(
				'name'          => 'qodef_video_type_meta',
				'type'          => 'select',
				'label'         => esc_html__( 'Video Type', 'peggi' ),
				'description'   => esc_html__( 'Choose video type', 'peggi' ),
				'parent'        => $video_post_format_meta_box,
				'default_value' => 'social_networks',
				'options'       => array(
					'social_networks' => esc_html__( 'Video Service', 'peggi' ),
					'self'            => esc_html__( 'Self Hosted', 'peggi' )
				)
			)
		);
		
		$qodef_video_embedded_container = peggi_select_add_admin_container(
			array(
				'parent' => $video_post_format_meta_box,
				'name'   => 'qodef_video_embedded_container'
			)
		);
		
		peggi_select_create_meta_box_field(
			array(
				'name'        => 'qodef_post_video_link_meta',
				'type'        => 'text',
				'label'       => esc_html__( 'Video URL', 'peggi' ),
				'description' => esc_html__( 'Enter Video URL', 'peggi' ),
				'parent'      => $qodef_video_embedded_container,
				'dependency' => array(
					'show' => array(
						'qodef_video_type_meta' => 'social_networks'
					)
				)
			)
		);
		
		peggi_select_create_meta_box_field(
			array(
				'name'        => 'qodef_post_video_custom_meta',
				'type'        => 'text',
				'label'       => esc_html__( 'Video MP4', 'peggi' ),
				'description' => esc_html__( 'Enter video URL for MP4 format', 'peggi' ),
				'parent'      => $qodef_video_embedded_container,
				'dependency' => array(
					'show' => array(
						'qodef_video_type_meta' => 'self'
					)
				)
			)
		);
		
		peggi_select_create_meta_box_field(
			array(
				'name'        => 'qodef_post_video_image_meta',
				'type'        => 'image',
				'label'       => esc_html__( 'Video Image', 'peggi' ),
				'description' => esc_html__( 'Enter video image', 'peggi' ),
				'parent'      => $qodef_video_embedded_container,
				'dependency' => array(
					'show' => array(
						'qodef_video_type_meta' => 'self'
					)
				)
			)
		);
	}
	
	add_action( 'peggi_select_action_meta_boxes_map', 'peggi_select_map_post_video_meta', 22 );
}