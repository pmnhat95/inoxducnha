<?php

if ( ! function_exists( 'peggi_select_map_post_link_meta' ) ) {
	function peggi_select_map_post_link_meta() {
		$link_post_format_meta_box = peggi_select_create_meta_box(
			array(
				'scope' => array( 'post' ),
				'title' => esc_html__( 'Link Post Format', 'peggi' ),
				'name'  => 'post_format_link_meta'
			)
		);
		
		peggi_select_create_meta_box_field(
			array(
				'name'        => 'qodef_post_link_link_meta',
				'type'        => 'text',
				'label'       => esc_html__( 'Link', 'peggi' ),
				'description' => esc_html__( 'Enter link', 'peggi' ),
				'parent'      => $link_post_format_meta_box
			)
		);
	}
	
	add_action( 'peggi_select_action_meta_boxes_map', 'peggi_select_map_post_link_meta', 24 );
}