<?php

if ( ! function_exists( 'peggi_select_register_image_gallery_widget' ) ) {
	/**
	 * Function that register image gallery widget
	 */
	function peggi_select_register_image_gallery_widget( $widgets ) {
		$widgets[] = 'PeggiSelectClassImageGalleryWidget';
		
		return $widgets;
	}
	
	add_filter( 'peggi_select_filter_register_widgets', 'peggi_select_register_image_gallery_widget' );
}