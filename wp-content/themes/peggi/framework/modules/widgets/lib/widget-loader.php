<?php

if ( ! function_exists( 'peggi_select_register_widgets' ) ) {
	function peggi_select_register_widgets() {
		$widgets = apply_filters( 'peggi_select_filter_register_widgets', $widgets = array() );
		
		foreach ( $widgets as $widget ) {
			register_widget( $widget );
		}
	}
	
	add_action( 'widgets_init', 'peggi_select_register_widgets' );
}