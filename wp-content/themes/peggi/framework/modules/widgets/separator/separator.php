<?php

class PeggiSelectClassSeparatorWidget extends PeggiSelectClassWidget {
	public function __construct() {
		parent::__construct(
			'qodef_separator_widget',
			esc_html__( 'Peggi Separator Widget', 'peggi' ),
			array( 'description' => esc_html__( 'Add a separator element to your widget areas', 'peggi' ) )
		);
		
		$this->setParams();
	}
	
	protected function setParams() {
		$this->params = array(
			array(
				'type'    => 'dropdown',
				'name'    => 'type',
				'title'   => esc_html__( 'Type', 'peggi' ),
				'options' => array(
					'normal'     => esc_html__( 'Normal', 'peggi' ),
					'full-width' => esc_html__( 'Full Width', 'peggi' )
				)
			),
			array(
				'type'    => 'dropdown',
				'name'    => 'position',
				'title'   => esc_html__( 'Position', 'peggi' ),
				'options' => array(
					'center' => esc_html__( 'Center', 'peggi' ),
					'left'   => esc_html__( 'Left', 'peggi' ),
					'right'  => esc_html__( 'Right', 'peggi' )
				)
			),
			array(
				'type'    => 'dropdown',
				'name'    => 'border_style',
				'title'   => esc_html__( 'Style', 'peggi' ),
				'options' => array(
					'solid'  => esc_html__( 'Solid', 'peggi' ),
					'dashed' => esc_html__( 'Dashed', 'peggi' ),
					'dotted' => esc_html__( 'Dotted', 'peggi' )
				)
			),
			array(
				'type'  => 'colorpicker',
				'name'  => 'color',
				'title' => esc_html__( 'Color', 'peggi' )
			),
			array(
				'type'  => 'textfield',
				'name'  => 'width',
				'title' => esc_html__( 'Width (px or %)', 'peggi' )
			),
			array(
				'type'  => 'textfield',
				'name'  => 'thickness',
				'title' => esc_html__( 'Thickness (px)', 'peggi' )
			),
			array(
				'type'  => 'textfield',
				'name'  => 'top_margin',
				'title' => esc_html__( 'Top Margin (px or %)', 'peggi' )
			),
			array(
				'type'  => 'textfield',
				'name'  => 'bottom_margin',
				'title' => esc_html__( 'Bottom Margin (px or %)', 'peggi' )
			)
		);
	}
	
	public function widget( $args, $instance ) {
		if ( ! is_array( $instance ) ) {
			$instance = array();
		}
		
		//prepare variables
		$params = '';
		
		//is instance empty?
		if ( is_array( $instance ) && count( $instance ) ) {
			//generate shortcode params
			foreach ( $instance as $key => $value ) {
				$params .= " $key='$value' ";
			}
		}
		
		echo '<div class="widget qodef-separator-widget">';
			echo do_shortcode( "[qodef_separator $params]" ); // XSS OK
		echo '</div>';
	}
}