<?php

if ( ! function_exists( 'peggi_select_register_separator_widget' ) ) {
	/**
	 * Function that register separator widget
	 */
	function peggi_select_register_separator_widget( $widgets ) {
		$widgets[] = 'PeggiSelectClassSeparatorWidget';
		
		return $widgets;
	}
	
	add_filter( 'peggi_select_filter_register_widgets', 'peggi_select_register_separator_widget' );
}