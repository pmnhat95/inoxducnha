<?php

if ( ! function_exists( 'peggi_select_register_custom_font_widget' ) ) {
	/**
	 * Function that register custom font widget
	 */
	function peggi_select_register_custom_font_widget( $widgets ) {
		$widgets[] = 'PeggiSelectClassCustomFontWidget';
		
		return $widgets;
	}
	
	add_filter( 'peggi_select_filter_register_widgets', 'peggi_select_register_custom_font_widget' );
}