<?php

if(!function_exists('peggi_select_register_sticky_sidebar_widget')) {
	/**
	 * Function that register sticky sidebar widget
	 */
	function peggi_select_register_sticky_sidebar_widget($widgets) {
		$widgets[] = 'PeggiSelectClassStickySidebar';
		
		return $widgets;
	}
	
	add_filter('peggi_select_filter_register_widgets', 'peggi_select_register_sticky_sidebar_widget');
}