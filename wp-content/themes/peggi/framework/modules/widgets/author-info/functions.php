<?php

if ( ! function_exists( 'peggi_select_register_author_info_widget' ) ) {
	/**
	 * Function that register author info widget
	 */
	function peggi_select_register_author_info_widget( $widgets ) {
		$widgets[] = 'PeggiSelectClassAuthorInfoWidget';
		
		return $widgets;
	}
	
	add_filter( 'peggi_select_filter_register_widgets', 'peggi_select_register_author_info_widget' );
}