<?php

class PeggiSelectClassAuthorInfoWidget extends PeggiSelectClassWidget {
	public function __construct() {
		parent::__construct(
			'qodef_author_info_widget',
			esc_html__( 'Peggi Author Info Widget', 'peggi' ),
			array( 'description' => esc_html__( 'Add author info element to widget areas', 'peggi' ) )
		);
		
		$this->setParams();
	}
	
	protected function setParams() {
		$this->params = array(
			array(
				'type'  => 'textfield',
				'name'  => 'extra_class',
				'title' => esc_html__( 'Custom CSS Class', 'peggi' )
			),
			array(
				'type'  => 'textfield',
				'name'  => 'author_username',
				'title' => esc_html__( 'Author Username', 'peggi' )
			),
			array(
				'type'        => 'textfield',
				'name'        => 'image',
				'title'       => esc_html__( 'Image URL', 'peggi' ),
				'description' => esc_html__( 'Add image url for author info background image', 'peggi' )
			),
		);
	}
	
	public function widget( $args, $instance ) {
		extract( $args );
		
		$extra_class = '';
		if ( ! empty( $instance['extra_class'] ) ) {
			$extra_class = $instance['extra_class'];
		}
		
		$authorID = 1;
		if ( ! empty( $instance['author_username'] ) ) {
			$author = get_user_by( 'login', $instance['author_username'] );
			
			if ( $author ) {
				$authorID = $author->ID;
			}
		}
		
		$user_meta   = get_userdata( $authorID );
		$author_name = get_the_author_meta( 'display_name', $authorID );
		$author_role = ! empty( $user_meta ) ? $user_meta->roles : '';
		
		$author_background_image = array();
		if ( ! empty( $instance['image'] ) ) {
			$author_background_image[] = 'background-image: url(' . $instance['image'] . ')';
		}
		?>
		
		<div class="widget qodef-author-info-widget <?php echo esc_html( $extra_class ); ?>" <?php echo peggi_select_get_inline_style( $author_background_image ); ?>>
			<div class="qodef-aiw-inner">
				<a itemprop="url" class="qodef-aiw-image" href="<?php echo esc_url( get_author_posts_url( $authorID ) ); ?>">
					<?php echo peggi_select_kses_img( get_avatar( $authorID, 102 ) ); ?>
				</a>
				<?php if ( ! empty( $author_name ) ) { ?>
					<h5 class="qodef-aiw-name"><?php echo wp_kses_post( $author_name ); ?></h5>
				<?php } ?>
				<?php if ( ! empty( $author_role ) && isset( $author_role[0] ) ) { ?>
					<p class="qodef-aiw-role"><?php echo wp_kses_post( $author_role[0] ); ?></p>
				<?php } ?>
			</div>
		</div>
		<?php
	}
}