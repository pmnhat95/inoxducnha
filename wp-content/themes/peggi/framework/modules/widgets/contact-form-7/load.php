<?php

if ( peggi_select_contact_form_7_installed() ) {
	include_once SELECT_FRAMEWORK_MODULES_ROOT_DIR . '/widgets/contact-form-7/contact-form-7.php';
	
	add_filter( 'peggi_select_filter_register_widgets', 'peggi_select_register_cf7_widget' );
}

if ( ! function_exists( 'peggi_select_register_cf7_widget' ) ) {
	/**
	 * Function that register cf7 widget
	 */
	function peggi_select_register_cf7_widget( $widgets ) {
		$widgets[] = 'PeggiSelectClassContactForm7Widget';
		
		return $widgets;
	}
}