<?php

if ( ! function_exists( 'peggi_select_register_social_icon_widget' ) ) {
	/**
	 * Function that register social icon widget
	 */
	function peggi_select_register_social_icon_widget( $widgets ) {
		$widgets[] = 'PeggiSelectClassSocialIconWidget';
		
		return $widgets;
	}
	
	add_filter( 'peggi_select_filter_register_widgets', 'peggi_select_register_social_icon_widget' );
}