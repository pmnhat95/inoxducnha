<?php

if ( ! function_exists( 'peggi_select_register_blog_list_widget' ) ) {
	/**
	 * Function that register blog list widget
	 */
	function peggi_select_register_blog_list_widget( $widgets ) {
		$widgets[] = 'PeggiSelectClassBlogListWidget';
		
		return $widgets;
	}
	
	add_filter( 'peggi_select_filter_register_widgets', 'peggi_select_register_blog_list_widget' );
}