<?php

if ( ! function_exists( 'peggi_select_register_button_widget' ) ) {
	/**
	 * Function that register button widget
	 */
	function peggi_select_register_button_widget( $widgets ) {
		$widgets[] = 'PeggiSelectClassButtonWidget';
		
		return $widgets;
	}
	
	add_filter( 'peggi_select_filter_register_widgets', 'peggi_select_register_button_widget' );
}