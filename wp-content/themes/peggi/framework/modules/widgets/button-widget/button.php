<?php

class PeggiSelectClassButtonWidget extends PeggiSelectClassWidget {
	public function __construct() {
		parent::__construct(
			'qodef_button_widget',
			esc_html__( 'Peggi Button Widget', 'peggi' ),
			array( 'description' => esc_html__( 'Add button element to widget areas', 'peggi' ) )
		);
		
		$this->setParams();
	}
	
	protected function setParams() {
		$this->params = array(
			array(
				'type'    => 'dropdown',
				'name'    => 'type',
				'title'   => esc_html__( 'Type', 'peggi' ),
				'options' => array(
					'solid'   => esc_html__( 'Solid', 'peggi' ),
					'outline' => esc_html__( 'Outline', 'peggi' ),
					'simple'  => esc_html__( 'Simple', 'peggi' )
				)
			),
			array(
				'type'        => 'dropdown',
				'name'        => 'size',
				'title'       => esc_html__( 'Size', 'peggi' ),
				'options'     => array(
					'small'  => esc_html__( 'Small', 'peggi' ),
					'medium' => esc_html__( 'Medium', 'peggi' ),
					'large'  => esc_html__( 'Large', 'peggi' ),
					'huge'   => esc_html__( 'Huge', 'peggi' )
				),
				'description' => esc_html__( 'This option is only available for solid and outline button type', 'peggi' )
			),
			array(
				'type'    => 'textfield',
				'name'    => 'text',
				'title'   => esc_html__( 'Text', 'peggi' ),
				'default' => esc_html__( 'Button Text', 'peggi' )
			),
			array(
				'type'  => 'textfield',
				'name'  => 'link',
				'title' => esc_html__( 'Link', 'peggi' )
			),
			array(
				'type'    => 'dropdown',
				'name'    => 'target',
				'title'   => esc_html__( 'Link Target', 'peggi' ),
				'options' => peggi_select_get_link_target_array()
			),
			array(
				'type'  => 'colorpicker',
				'name'  => 'color',
				'title' => esc_html__( 'Color', 'peggi' )
			),
			array(
				'type'  => 'colorpicker',
				'name'  => 'hover_color',
				'title' => esc_html__( 'Hover Color', 'peggi' )
			),
			array(
				'type'        => 'colorpicker',
				'name'        => 'background_color',
				'title'       => esc_html__( 'Background Color', 'peggi' ),
				'description' => esc_html__( 'This option is only available for solid button type', 'peggi' )
			),
			array(
				'type'        => 'colorpicker',
				'name'        => 'hover_background_color',
				'title'       => esc_html__( 'Hover Background Color', 'peggi' ),
				'description' => esc_html__( 'This option is only available for solid button type', 'peggi' )
			),
			array(
				'type'        => 'colorpicker',
				'name'        => 'border_color',
				'title'       => esc_html__( 'Border Color', 'peggi' ),
				'description' => esc_html__( 'This option is only available for solid and outline button type', 'peggi' )
			),
			array(
				'type'        => 'colorpicker',
				'name'        => 'hover_border_color',
				'title'       => esc_html__( 'Hover Border Color', 'peggi' ),
				'description' => esc_html__( 'This option is only available for solid and outline button type', 'peggi' )
			),
			array(
				'type'        => 'textfield',
				'name'        => 'margin',
				'title'       => esc_html__( 'Margin', 'peggi' ),
				'description' => esc_html__( 'Insert margin in format: top right bottom left (e.g. 10px 5px 10px 5px)', 'peggi' )
			)
		);
	}
	
	public function widget( $args, $instance ) {
		$params = '';
		
		if ( ! is_array( $instance ) ) {
			$instance = array();
		}
		
		// Filter out all empty params
		$instance = array_filter( $instance, function ( $array_value ) {
			return trim( $array_value ) != '';
		} );
		
		// Default values
		if ( ! isset( $instance['text'] ) ) {
			$instance['text'] = 'Button Text';
		}
		
		// Generate shortcode params
		foreach ( $instance as $key => $value ) {
			$params .= " $key='$value' ";
		}
		
		echo '<div class="widget qodef-button-widget">';
			echo do_shortcode( "[qodef_button $params]" ); // XSS OK
		echo '</div>';
	}
}