<?php

if ( ! function_exists( 'peggi_select_register_search_opener_widget' ) ) {
	/**
	 * Function that register search opener widget
	 */
	function peggi_select_register_search_opener_widget( $widgets ) {
		$widgets[] = 'PeggiSelectClassSearchOpener';
		
		return $widgets;
	}
	
	add_filter( 'peggi_select_filter_register_widgets', 'peggi_select_register_search_opener_widget' );
}