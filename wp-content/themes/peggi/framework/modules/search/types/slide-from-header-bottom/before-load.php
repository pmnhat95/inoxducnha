<?php

if ( ! function_exists( 'peggi_select_set_search_slide_from_hb_global_option' ) ) {
    /**
     * This function set search type value for search options map
     */
    function peggi_select_set_search_slide_from_hb_global_option( $search_type_options ) {
        $search_type_options['slide-from-header-bottom'] = esc_html__( 'Slide From Header Bottom', 'peggi' );

        return $search_type_options;
    }

    add_filter( 'peggi_select_filter_search_type_global_option', 'peggi_select_set_search_slide_from_hb_global_option' );
}