<form action="<?php echo esc_url( home_url( '/' ) ); ?>" class="qodef-search-page-form" method="get">
	<h5 class="qodef-search-title"><?php esc_html_e( 'Tìm kiếm mới:', 'peggi' ); ?></h5>
	<div class="qodef-form-holder">
		<div class="qodef-column-left">
			<input type="text" name="s" class="qodef-search-field" autocomplete="off" value="" placeholder="<?php esc_attr_e( 'Viết ở đây', 'peggi' ); ?>"/>
		</div>
		<div class="qodef-column-right">
			<button type="submit" class="qodef-search-submit"><span class="lnr lnr-magnifier"></span></button>
		</div>
	</div>
	<div class="qodef-search-label">
		<?php esc_html_e( 'Nếu bạn không hài lòng với kết quả dưới đây, vui lòng thực hiện một tìm kiếm khác', 'peggi' ); ?>
	</div>
</form>