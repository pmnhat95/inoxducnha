<?php

if ( ! function_exists( 'peggi_select_search_opener_icon_size' ) ) {
	function peggi_select_search_opener_icon_size() {
		$icon_size = peggi_select_options()->getOptionValue( 'header_search_icon_size' );
		
		if ( ! empty( $icon_size ) ) {
			echo peggi_select_dynamic_css( '.qodef-search-opener', array(
				'font-size' => peggi_select_filter_px( $icon_size ) . 'px'
			) );
		}
	}
	
	add_action( 'peggi_select_action_style_dynamic', 'peggi_select_search_opener_icon_size' );
}

if ( ! function_exists( 'peggi_select_search_opener_icon_colors' ) ) {
	function peggi_select_search_opener_icon_colors() {
		$icon_color       = peggi_select_options()->getOptionValue( 'header_search_icon_color' );
		$icon_hover_color = peggi_select_options()->getOptionValue( 'header_search_icon_hover_color' );
		
		if ( ! empty( $icon_color ) ) {
			echo peggi_select_dynamic_css( '.qodef-search-opener', array(
				'color' => $icon_color
			) );
		}
		
		if ( ! empty( $icon_hover_color ) ) {
			echo peggi_select_dynamic_css( '.qodef-search-opener:hover', array(
				'color' => $icon_hover_color
			) );
		}
	}
	
	add_action( 'peggi_select_action_style_dynamic', 'peggi_select_search_opener_icon_colors' );
}

if ( ! function_exists( 'peggi_select_search_opener_text_styles' ) ) {
	function peggi_select_search_opener_text_styles() {
		$item_styles = peggi_select_get_typography_styles( 'search_icon_text' );
		
		$item_selector = array(
			'.qodef-search-icon-text'
		);
		
		echo peggi_select_dynamic_css( $item_selector, $item_styles );
		
		$text_hover_color = peggi_select_options()->getOptionValue( 'search_icon_text_color_hover' );
		
		if ( ! empty( $text_hover_color ) ) {
			echo peggi_select_dynamic_css( '.qodef-search-opener:hover .qodef-search-icon-text', array(
				'color' => $text_hover_color
			) );
		}
	}
	
	add_action( 'peggi_select_action_style_dynamic', 'peggi_select_search_opener_text_styles' );
}

if ( ! function_exists( 'peggi_select_search_general_styles' ) ) {
	/**
	 * Generates general custom styles for search area
	 */
	function peggi_select_search_general_styles() {
		$item_styles      = array();
		$background_image = peggi_select_options()->getOptionValue( 'search_background_image' );
		
		if ( ! empty( $background_image ) ) {
			$item_styles['background-image'] = 'url(' . esc_url( $background_image ) . ')';
		}
		
		if ( ! empty( $item_styles ) ) {
			echo peggi_select_dynamic_css( '.qodef-fullscreen-search-holder .qodef-fullscreen-search-table', $item_styles );
		}
	}
	
	add_action( 'peggi_select_action_style_dynamic', 'peggi_select_search_general_styles' );
}