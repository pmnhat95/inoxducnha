<?php

if ( ! function_exists( 'peggi_select_disable_wpml_css' ) ) {
	function peggi_select_disable_wpml_css() {
		define( 'ICL_DONT_LOAD_LANGUAGE_SELECTOR_CSS', true );
	}
	
	add_action( 'after_setup_theme', 'peggi_select_disable_wpml_css' );
}