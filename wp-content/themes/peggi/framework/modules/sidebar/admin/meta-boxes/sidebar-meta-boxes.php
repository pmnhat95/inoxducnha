<?php

if ( ! function_exists( 'peggi_select_map_sidebar_meta' ) ) {
	function peggi_select_map_sidebar_meta() {
		$qodef_sidebar_meta_box = peggi_select_create_meta_box(
			array(
				'scope' => apply_filters( 'peggi_select_filter_set_scope_for_meta_boxes', array( 'page' ), 'sidebar_meta' ),
				'title' => esc_html__( 'Sidebar', 'peggi' ),
				'name'  => 'sidebar_meta'
			)
		);
		
		peggi_select_create_meta_box_field(
			array(
				'name'        => 'qodef_sidebar_layout_meta',
				'type'        => 'select',
				'label'       => esc_html__( 'Sidebar Layout', 'peggi' ),
				'description' => esc_html__( 'Choose the sidebar layout', 'peggi' ),
				'parent'      => $qodef_sidebar_meta_box,
                'options'       => peggi_select_get_custom_sidebars_options( true )
			)
		);
		
		$qodef_custom_sidebars = peggi_select_get_custom_sidebars();
		if ( count( $qodef_custom_sidebars ) > 0 ) {
			peggi_select_create_meta_box_field(
				array(
					'name'        => 'qodef_custom_sidebar_area_meta',
					'type'        => 'selectblank',
					'label'       => esc_html__( 'Choose Widget Area in Sidebar', 'peggi' ),
					'description' => esc_html__( 'Choose Custom Widget area to display in Sidebar"', 'peggi' ),
					'parent'      => $qodef_sidebar_meta_box,
					'options'     => $qodef_custom_sidebars,
					'args'        => array(
						'select2' => true
					)
				)
			);
		}
	}
	
	add_action( 'peggi_select_action_meta_boxes_map', 'peggi_select_map_sidebar_meta', 31 );
}