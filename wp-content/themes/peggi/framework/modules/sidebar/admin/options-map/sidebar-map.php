<?php

if ( ! function_exists( 'peggi_select_sidebar_options_map' ) ) {
	function peggi_select_sidebar_options_map() {
		
		peggi_select_add_admin_page(
			array(
				'slug'  => '_sidebar_page',
				'title' => esc_html__( 'Sidebar Area', 'peggi' ),
				'icon'  => 'fa fa-indent'
			)
		);
		
		$sidebar_panel = peggi_select_add_admin_panel(
			array(
				'title' => esc_html__( 'Sidebar Area', 'peggi' ),
				'name'  => 'sidebar',
				'page'  => '_sidebar_page'
			)
		);
		
		peggi_select_add_admin_field( array(
			'name'          => 'sidebar_layout',
			'type'          => 'select',
			'label'         => esc_html__( 'Sidebar Layout', 'peggi' ),
			'description'   => esc_html__( 'Choose a sidebar layout for pages', 'peggi' ),
			'parent'        => $sidebar_panel,
			'default_value' => 'no-sidebar',
            'options'       => peggi_select_get_custom_sidebars_options()
		) );
		
		$peggi_custom_sidebars = peggi_select_get_custom_sidebars();
		if ( count( $peggi_custom_sidebars ) > 0 ) {
			peggi_select_add_admin_field( array(
				'name'        => 'custom_sidebar_area',
				'type'        => 'selectblank',
				'label'       => esc_html__( 'Sidebar to Display', 'peggi' ),
				'description' => esc_html__( 'Choose a sidebar to display on pages. Default sidebar is "Sidebar"', 'peggi' ),
				'parent'      => $sidebar_panel,
				'options'     => $peggi_custom_sidebars,
				'args'        => array(
					'select2' => true
				)
			) );
		}
	}
	
	add_action( 'peggi_select_action_options_map', 'peggi_select_sidebar_options_map', 9 );
}