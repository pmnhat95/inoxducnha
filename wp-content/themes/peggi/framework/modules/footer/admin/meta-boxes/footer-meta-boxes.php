<?php

if ( ! function_exists( 'peggi_select_map_footer_meta' ) ) {
	function peggi_select_map_footer_meta() {
		
		$footer_meta_box = peggi_select_create_meta_box(
			array(
				'scope' => apply_filters( 'peggi_select_filter_set_scope_for_meta_boxes', array( 'page', 'post' ), 'footer_meta' ),
				'title' => esc_html__( 'Footer', 'peggi' ),
				'name'  => 'footer_meta'
			)
		);
		
		peggi_select_create_meta_box_field(
			array(
				'name'          => 'qodef_disable_footer_meta',
				'type'          => 'select',
				'default_value' => '',
				'label'         => esc_html__( 'Disable Footer for this Page', 'peggi' ),
				'description'   => esc_html__( 'Enabling this option will hide footer on this page', 'peggi' ),
				'options'       => peggi_select_get_yes_no_select_array(),
				'parent'        => $footer_meta_box
			)
		);
		
		$show_footer_meta_container = peggi_select_add_admin_container(
			array(
				'name'       => 'qodef_show_footer_meta_container',
				'parent'     => $footer_meta_box,
				'dependency' => array(
					'hide' => array(
						'qodef_disable_footer_meta' => 'yes'
					)
				)
			)
		);
		
			peggi_select_create_meta_box_field(
				array(
					'name'          => 'qodef_footer_in_grid_meta',
					'type'          => 'select',
					'default_value' => '',
					'label'         => esc_html__( 'Footer in Grid', 'peggi' ),
					'description'   => esc_html__( 'Enabling this option will place Footer content in grid', 'peggi' ),
					'options'       => peggi_select_get_yes_no_select_array(),
					'parent'        => $show_footer_meta_container
				)
			);
			
			peggi_select_create_meta_box_field(
				array(
					'name'          => 'qodef_uncovering_footer_meta',
					'type'          => 'select',
					'default_value' => '',
					'label'         => esc_html__( 'Uncovering Footer', 'peggi' ),
					'description'   => esc_html__( 'Enabling this option will make Footer gradually appear on scroll', 'peggi' ),
					'options'       => peggi_select_get_yes_no_select_array(),
					'parent'        => $show_footer_meta_container
				)
			);
		
			peggi_select_create_meta_box_field(
				array(
					'name'          => 'qodef_show_footer_top_meta',
					'type'          => 'select',
					'default_value' => '',
					'label'         => esc_html__( 'Show Footer Top', 'peggi' ),
					'description'   => esc_html__( 'Enabling this option will show Footer Top area', 'peggi' ),
					'options'       => peggi_select_get_yes_no_select_array(),
					'parent'        => $show_footer_meta_container
				)
			);
		
			peggi_select_create_meta_box_field(
				array(
					'name'        => 'qodef_footer_top_background_color_meta',
					'type'        => 'color',
					'label'       => esc_html__( 'Footer Top Background Color', 'peggi' ),
					'description' => esc_html__( 'Set background color for top footer area', 'peggi' ),
					'parent'      => $show_footer_meta_container,
					'dependency' => array(
						'show' => array(
							'qodef_show_footer_top_meta' => array( '', 'yes' )
						)
					)
				)
			);
			
			peggi_select_create_meta_box_field(
				array(
					'name'          => 'qodef_show_footer_bottom_meta',
					'type'          => 'select',
					'default_value' => '',
					'label'         => esc_html__( 'Show Footer Bottom', 'peggi' ),
					'description'   => esc_html__( 'Enabling this option will show Footer Bottom area', 'peggi' ),
					'options'       => peggi_select_get_yes_no_select_array(),
					'parent'        => $show_footer_meta_container
				)
			);
		
			peggi_select_create_meta_box_field(
				array(
					'name'        => 'qodef_footer_bottom_background_color_meta',
					'type'        => 'color',
					'label'       => esc_html__( 'Footer Bottom Background Color', 'peggi' ),
					'description' => esc_html__( 'Set background color for bottom footer area', 'peggi' ),
					'parent'      => $show_footer_meta_container,
					'dependency' => array(
						'show' => array(
							'qodef_show_footer_bottom_meta' => array( '', 'yes' )
						)
					)
				)
			);
	}
	
	add_action( 'peggi_select_action_meta_boxes_map', 'peggi_select_map_footer_meta', 70 );
}