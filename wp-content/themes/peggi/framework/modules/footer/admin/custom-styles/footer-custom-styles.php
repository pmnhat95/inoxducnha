<?php

if ( ! function_exists( 'peggi_select_footer_top_general_styles' ) ) {
	/**
	 * Generates general custom styles for footer top area
	 */
	function peggi_select_footer_top_general_styles() {
		$item_styles      = array();
		$background_color = peggi_select_options()->getOptionValue( 'footer_top_background_color' );
		$background_image = peggi_select_options()->getOptionValue( 'footer_background_image' );
		
		if ( ! empty( $background_color ) ) {
			$item_styles['background-color'] = $background_color;
		}
		
		if ( ! empty( $background_image ) ) {
			$item_styles['background-image'] = 'url(' . esc_url( $background_image ) . ')';
		}
		
		echo peggi_select_dynamic_css( '.qodef-page-footer .qodef-footer-top-holder', $item_styles );
	}
	
	add_action( 'peggi_select_action_style_dynamic', 'peggi_select_footer_top_general_styles' );
}

if ( ! function_exists( 'peggi_select_footer_bottom_general_styles' ) ) {
	/**
	 * Generates general custom styles for footer bottom area
	 */
	function peggi_select_footer_bottom_general_styles() {
		$item_styles      = array();
		$background_color = peggi_select_options()->getOptionValue( 'footer_bottom_background_color' );
		
		if ( ! empty( $background_color ) ) {
			$item_styles['background-color'] = $background_color;
		}
		
		echo peggi_select_dynamic_css( '.qodef-page-footer .qodef-footer-bottom-holder', $item_styles );
	}
	
	add_action( 'peggi_select_action_style_dynamic', 'peggi_select_footer_bottom_general_styles' );
}