<?php

if ( ! function_exists( 'peggi_select_footer_options_map' ) ) {
	function peggi_select_footer_options_map() {

		peggi_select_add_admin_page(
			array(
				'slug'  => '_footer_page',
				'title' => esc_html__( 'Footer', 'peggi' ),
				'icon'  => 'fa fa-sort-amount-asc'
			)
		);

		$footer_panel = peggi_select_add_admin_panel(
			array(
				'title' => esc_html__( 'Footer', 'peggi' ),
				'name'  => 'footer',
				'page'  => '_footer_page'
			)
		);

		peggi_select_add_admin_field(
			array(
				'type'          => 'yesno',
				'name'          => 'footer_in_grid',
				'default_value' => 'yes',
				'label'         => esc_html__( 'Footer in Grid', 'peggi' ),
				'description'   => esc_html__( 'Enabling this option will place Footer content in grid', 'peggi' ),
				'parent'        => $footer_panel
			)
		);

        peggi_select_add_admin_field(
            array(
                'type'          => 'yesno',
                'name'          => 'uncovering_footer',
                'default_value' => 'no',
                'label'         => esc_html__( 'Uncovering Footer', 'peggi' ),
                'description'   => esc_html__( 'Enabling this option will make Footer gradually appear on scroll', 'peggi' ),
                'parent'        => $footer_panel,
            )
        );

		peggi_select_add_admin_field(
			array(
				'type'          => 'yesno',
				'name'          => 'show_footer_top',
				'default_value' => 'yes',
				'label'         => esc_html__( 'Show Footer Top', 'peggi' ),
				'description'   => esc_html__( 'Enabling this option will show Footer Top area', 'peggi' ),
				'parent'        => $footer_panel,
			)
		);
		
		$show_footer_top_container = peggi_select_add_admin_container(
			array(
				'name'       => 'show_footer_top_container',
				'parent'     => $footer_panel,
				'dependency' => array(
					'show' => array(
						'show_footer_top' => 'yes'
					)
				)
			)
		);

		peggi_select_add_admin_field(
			array(
				'type'          => 'select',
				'name'          => 'footer_top_columns',
				'parent'        => $show_footer_top_container,
				'default_value' => '12',
				'label'         => esc_html__( 'Footer Top Columns', 'peggi' ),
				'description'   => esc_html__( 'Choose number of columns for Footer Top area', 'peggi' ),
				'options'       => array(
					'12' => '1',
					'6 6' => '2',
					'4 4 4' => '3',
                    '3 3 6' => '3 (25% + 25% + 50%)',
					'3 3 3 3' => '4'
				)
			)
		);

		peggi_select_add_admin_field(
			array(
				'type'          => 'select',
				'name'          => 'footer_top_columns_alignment',
				'default_value' => 'center',
				'label'         => esc_html__( 'Footer Top Columns Alignment', 'peggi' ),
				'description'   => esc_html__( 'Text Alignment in Footer Columns', 'peggi' ),
				'options'       => array(
					''       => esc_html__( 'Default', 'peggi' ),
					'left'   => esc_html__( 'Left', 'peggi' ),
					'center' => esc_html__( 'Center', 'peggi' ),
					'right'  => esc_html__( 'Right', 'peggi' )
				),
				'parent'        => $show_footer_top_container,
			)
		);

		peggi_select_add_admin_field(
			array(
				'name'        => 'footer_top_background_color',
				'type'        => 'color',
				'label'       => esc_html__( 'Background Color', 'peggi' ),
				'description' => esc_html__( 'Set background color for top footer area', 'peggi' ),
				'parent'      => $show_footer_top_container
			)
		);
		
		peggi_select_add_admin_field(
			array(
				'name'        => 'footer_background_image',
				'type'        => 'image',
				'label'       => esc_html__( 'Footer Background Image', 'peggi' ),
				'description' => esc_html__( 'Choose an Background Image for Footer top', 'peggi' ),
				'parent'      => $show_footer_top_container
			)
		);

		peggi_select_add_admin_field(
			array(
				'type'          => 'yesno',
				'name'          => 'show_footer_bottom',
				'default_value' => 'yes',
				'label'         => esc_html__( 'Show Footer Bottom', 'peggi' ),
				'description'   => esc_html__( 'Enabling this option will show Footer Bottom area', 'peggi' ),
				'parent'        => $footer_panel,
			)
		);

		$show_footer_bottom_container = peggi_select_add_admin_container(
			array(
				'name'            => 'show_footer_bottom_container',
				'parent'          => $footer_panel,
				'dependency' => array(
					'show' => array(
						'show_footer_bottom'  => 'yes'
					)
				)
			)
		);

		peggi_select_add_admin_field(
			array(
				'type'          => 'select',
				'name'          => 'footer_bottom_columns',
				'default_value' => '12',
				'label'         => esc_html__( 'Footer Bottom Columns', 'peggi' ),
				'description'   => esc_html__( 'Choose number of columns for Footer Bottom area', 'peggi' ),
				'options'       => array(
					'12' => '1',
					'6 6' => '2',
					'4 4 4' => '3'
				),
				'parent'        => $show_footer_bottom_container,
			)
		);

		peggi_select_add_admin_field(
			array(
				'name'        => 'footer_bottom_background_color',
				'type'        => 'color',
				'label'       => esc_html__( 'Background Color', 'peggi' ),
				'description' => esc_html__( 'Set background color for bottom footer area', 'peggi' ),
				'parent'      => $show_footer_bottom_container
			)
		);
	}

	add_action( 'peggi_select_action_options_map', 'peggi_select_footer_options_map', 5 );
}