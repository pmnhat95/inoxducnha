<?php

if ( ! function_exists( 'peggi_select_general_options_map' ) ) {
	/**
	 * General options page
	 */
	function peggi_select_general_options_map() {
		
		peggi_select_add_admin_page(
			array(
				'slug'  => '',
				'title' => esc_html__( 'General', 'peggi' ),
				'icon'  => 'fa fa-institution'
			)
		);
		
		$panel_design_style = peggi_select_add_admin_panel(
			array(
				'page'  => '',
				'name'  => 'panel_design_style',
				'title' => esc_html__( 'Design Style', 'peggi' )
			)
		);
		
		peggi_select_add_admin_field(
			array(
				'name'          => 'google_fonts',
				'type'          => 'font',
				'default_value' => '-1',
				'label'         => esc_html__( 'Google Font Family', 'peggi' ),
				'description'   => esc_html__( 'Choose a default Google font for your site', 'peggi' ),
				'parent'        => $panel_design_style
			)
		);
		
		peggi_select_add_admin_field(
			array(
				'name'          => 'additional_google_fonts',
				'type'          => 'yesno',
				'default_value' => 'no',
				'label'         => esc_html__( 'Additional Google Fonts', 'peggi' ),
				'parent'        => $panel_design_style
			)
		);
		
		$additional_google_fonts_container = peggi_select_add_admin_container(
			array(
				'parent'          => $panel_design_style,
				'name'            => 'additional_google_fonts_container',
				'dependency' => array(
					'show' => array(
						'additional_google_fonts'  => 'yes'
					)
				)
			)
		);
		
		peggi_select_add_admin_field(
			array(
				'name'          => 'additional_google_font1',
				'type'          => 'font',
				'default_value' => '-1',
				'label'         => esc_html__( 'Font Family', 'peggi' ),
				'description'   => esc_html__( 'Choose additional Google font for your site', 'peggi' ),
				'parent'        => $additional_google_fonts_container
			)
		);
		
		peggi_select_add_admin_field(
			array(
				'name'          => 'additional_google_font2',
				'type'          => 'font',
				'default_value' => '-1',
				'label'         => esc_html__( 'Font Family', 'peggi' ),
				'description'   => esc_html__( 'Choose additional Google font for your site', 'peggi' ),
				'parent'        => $additional_google_fonts_container
			)
		);
		
		peggi_select_add_admin_field(
			array(
				'name'          => 'additional_google_font3',
				'type'          => 'font',
				'default_value' => '-1',
				'label'         => esc_html__( 'Font Family', 'peggi' ),
				'description'   => esc_html__( 'Choose additional Google font for your site', 'peggi' ),
				'parent'        => $additional_google_fonts_container
			)
		);
		
		peggi_select_add_admin_field(
			array(
				'name'          => 'additional_google_font4',
				'type'          => 'font',
				'default_value' => '-1',
				'label'         => esc_html__( 'Font Family', 'peggi' ),
				'description'   => esc_html__( 'Choose additional Google font for your site', 'peggi' ),
				'parent'        => $additional_google_fonts_container
			)
		);
		
		peggi_select_add_admin_field(
			array(
				'name'          => 'additional_google_font5',
				'type'          => 'font',
				'default_value' => '-1',
				'label'         => esc_html__( 'Font Family', 'peggi' ),
				'description'   => esc_html__( 'Choose additional Google font for your site', 'peggi' ),
				'parent'        => $additional_google_fonts_container
			)
		);
		
		peggi_select_add_admin_field(
			array(
				'name'          => 'google_font_weight',
				'type'          => 'checkboxgroup',
				'default_value' => '',
				'label'         => esc_html__( 'Google Fonts Style & Weight', 'peggi' ),
				'description'   => esc_html__( 'Choose a default Google font weights for your site. Impact on page load time', 'peggi' ),
				'parent'        => $panel_design_style,
				'options'       => array(
					'100'  => esc_html__( '100 Thin', 'peggi' ),
					'100i' => esc_html__( '100 Thin Italic', 'peggi' ),
					'200'  => esc_html__( '200 Extra-Light', 'peggi' ),
					'200i' => esc_html__( '200 Extra-Light Italic', 'peggi' ),
					'300'  => esc_html__( '300 Light', 'peggi' ),
					'300i' => esc_html__( '300 Light Italic', 'peggi' ),
					'400'  => esc_html__( '400 Regular', 'peggi' ),
					'400i' => esc_html__( '400 Regular Italic', 'peggi' ),
					'500'  => esc_html__( '500 Medium', 'peggi' ),
					'500i' => esc_html__( '500 Medium Italic', 'peggi' ),
					'600'  => esc_html__( '600 Semi-Bold', 'peggi' ),
					'600i' => esc_html__( '600 Semi-Bold Italic', 'peggi' ),
					'700'  => esc_html__( '700 Bold', 'peggi' ),
					'700i' => esc_html__( '700 Bold Italic', 'peggi' ),
					'800'  => esc_html__( '800 Extra-Bold', 'peggi' ),
					'800i' => esc_html__( '800 Extra-Bold Italic', 'peggi' ),
					'900'  => esc_html__( '900 Ultra-Bold', 'peggi' ),
					'900i' => esc_html__( '900 Ultra-Bold Italic', 'peggi' )
				)
			)
		);
		
		peggi_select_add_admin_field(
			array(
				'name'          => 'google_font_subset',
				'type'          => 'checkboxgroup',
				'default_value' => '',
				'label'         => esc_html__( 'Google Fonts Subset', 'peggi' ),
				'description'   => esc_html__( 'Choose a default Google font subsets for your site', 'peggi' ),
				'parent'        => $panel_design_style,
				'options'       => array(
					'latin'        => esc_html__( 'Latin', 'peggi' ),
					'latin-ext'    => esc_html__( 'Latin Extended', 'peggi' ),
					'cyrillic'     => esc_html__( 'Cyrillic', 'peggi' ),
					'cyrillic-ext' => esc_html__( 'Cyrillic Extended', 'peggi' ),
					'greek'        => esc_html__( 'Greek', 'peggi' ),
					'greek-ext'    => esc_html__( 'Greek Extended', 'peggi' ),
					'vietnamese'   => esc_html__( 'Vietnamese', 'peggi' )
				)
			)
		);
		
		peggi_select_add_admin_field(
			array(
				'name'        => 'first_color',
				'type'        => 'color',
				'label'       => esc_html__( 'First Main Color', 'peggi' ),
				'description' => esc_html__( 'Choose the most dominant theme color. Default color is #00bbb3', 'peggi' ),
				'parent'      => $panel_design_style
			)
		);
		
		peggi_select_add_admin_field(
			array(
				'name'        => 'page_background_color',
				'type'        => 'color',
				'label'       => esc_html__( 'Page Background Color', 'peggi' ),
				'description' => esc_html__( 'Choose the background color for page content. Default color is #ffffff', 'peggi' ),
				'parent'      => $panel_design_style
			)
		);
		
		peggi_select_add_admin_field(
			array(
				'name'        => 'page_background_image',
				'type'        => 'image',
				'label'       => esc_html__( 'Page Background Image', 'peggi' ),
				'description' => esc_html__( 'Choose the background image for page content', 'peggi' ),
				'parent'      => $panel_design_style
			)
		);
		
		peggi_select_add_admin_field(
			array(
				'name'          => 'page_background_image_repeat',
				'type'          => 'yesno',
				'default_value' => 'no',
				'label'         => esc_html__( 'Page Background Image Repeat', 'peggi' ),
				'description'   => esc_html__( 'Enabling this option will set page background image as pattern in otherwise will be cover background image', 'peggi' ),
				'parent'        => $panel_design_style
			)
		);
		
		peggi_select_add_admin_field(
			array(
				'name'        => 'selection_color',
				'type'        => 'color',
				'label'       => esc_html__( 'Text Selection Color', 'peggi' ),
				'description' => esc_html__( 'Choose the color users see when selecting text', 'peggi' ),
				'parent'      => $panel_design_style
			)
		);
		
		/***************** Passepartout Layout - begin **********************/
		
		peggi_select_add_admin_field(
			array(
				'name'          => 'boxed',
				'type'          => 'yesno',
				'default_value' => 'no',
				'label'         => esc_html__( 'Boxed Layout', 'peggi' ),
				'parent'        => $panel_design_style
			)
		);
		
			$boxed_container = peggi_select_add_admin_container(
				array(
					'parent'          => $panel_design_style,
					'name'            => 'boxed_container',
					'dependency' => array(
						'show' => array(
							'boxed'  => 'yes'
						)
					)
				)
			);
		
				peggi_select_add_admin_field(
					array(
						'name'        => 'page_background_color_in_box',
						'type'        => 'color',
						'label'       => esc_html__( 'Page Background Color', 'peggi' ),
						'description' => esc_html__( 'Choose the page background color outside box', 'peggi' ),
						'parent'      => $boxed_container
					)
				);
				
				peggi_select_add_admin_field(
					array(
						'name'        => 'boxed_background_image',
						'type'        => 'image',
						'label'       => esc_html__( 'Background Image', 'peggi' ),
						'description' => esc_html__( 'Choose an image to be displayed in background', 'peggi' ),
						'parent'      => $boxed_container
					)
				);
				
				peggi_select_add_admin_field(
					array(
						'name'        => 'boxed_pattern_background_image',
						'type'        => 'image',
						'label'       => esc_html__( 'Background Pattern', 'peggi' ),
						'description' => esc_html__( 'Choose an image to be used as background pattern', 'peggi' ),
						'parent'      => $boxed_container
					)
				);
				
				peggi_select_add_admin_field(
					array(
						'name'          => 'boxed_background_image_attachment',
						'type'          => 'select',
						'default_value' => '',
						'label'         => esc_html__( 'Background Image Attachment', 'peggi' ),
						'description'   => esc_html__( 'Choose background image attachment', 'peggi' ),
						'parent'        => $boxed_container,
						'options'       => array(
							''       => esc_html__( 'Default', 'peggi' ),
							'fixed'  => esc_html__( 'Fixed', 'peggi' ),
							'scroll' => esc_html__( 'Scroll', 'peggi' )
						)
					)
				);
		
		/***************** Boxed Layout - end **********************/
		
		/***************** Passepartout Layout - begin **********************/
		
		peggi_select_add_admin_field(
			array(
				'name'          => 'paspartu',
				'type'          => 'yesno',
				'default_value' => 'no',
				'label'         => esc_html__( 'Passepartout', 'peggi' ),
				'description'   => esc_html__( 'Enabling this option will display passepartout around site content', 'peggi' ),
				'parent'        => $panel_design_style
			)
		);
		
			$paspartu_container = peggi_select_add_admin_container(
				array(
					'parent'          => $panel_design_style,
					'name'            => 'paspartu_container',
					'dependency' => array(
						'show' => array(
							'paspartu'  => 'yes'
						)
					)
				)
			);
		
				peggi_select_add_admin_field(
					array(
						'name'        => 'paspartu_color',
						'type'        => 'color',
						'label'       => esc_html__( 'Passepartout Color', 'peggi' ),
						'description' => esc_html__( 'Choose passepartout color, default value is #ffffff', 'peggi' ),
						'parent'      => $paspartu_container
					)
				);
				
				peggi_select_add_admin_field(
					array(
						'name'        => 'paspartu_width',
						'type'        => 'text',
						'label'       => esc_html__( 'Passepartout Size', 'peggi' ),
						'description' => esc_html__( 'Enter size amount for passepartout', 'peggi' ),
						'parent'      => $paspartu_container,
						'args'        => array(
							'col_width' => 2,
							'suffix'    => 'px or %'
						)
					)
				);
		
				peggi_select_add_admin_field(
					array(
						'name'        => 'paspartu_responsive_width',
						'type'        => 'text',
						'label'       => esc_html__( 'Responsive Passepartout Size', 'peggi' ),
						'description' => esc_html__( 'Enter size amount for passepartout for smaller screens (tablets and mobiles view)', 'peggi' ),
						'parent'      => $paspartu_container,
						'args'        => array(
							'col_width' => 2,
							'suffix'    => 'px or %'
						)
					)
				);
				
				peggi_select_add_admin_field(
					array(
						'parent'        => $paspartu_container,
						'type'          => 'yesno',
						'default_value' => 'no',
						'name'          => 'disable_top_paspartu',
						'label'         => esc_html__( 'Disable Top Passepartout', 'peggi' )
					)
				);
		
				peggi_select_add_admin_field(
					array(
						'parent'        => $paspartu_container,
						'type'          => 'yesno',
						'default_value' => 'no',
						'name'          => 'enable_fixed_paspartu',
						'label'         => esc_html__( 'Enable Fixed Passepartout', 'peggi' ),
						'description' => esc_html__( 'Enabling this option will set fixed passepartout for your screens', 'peggi' )
					)
				);
		
		/***************** Passepartout Layout - end **********************/
		
		/***************** Content Layout - begin **********************/
		
		peggi_select_add_admin_field(
			array(
				'name'          => 'initial_content_width',
				'type'          => 'select',
				'default_value' => '',
				'label'         => esc_html__( 'Initial Width of Content', 'peggi' ),
				'description'   => esc_html__( 'Choose the initial width of content which is in grid (Applies to pages set to "Default Template" and rows set to "In Grid")', 'peggi' ),
				'parent'        => $panel_design_style,
				'options'       => array(
					'qodef-grid-1100' => esc_html__( '1100px - default', 'peggi' ),
					'qodef-grid-1300' => esc_html__( '1300px', 'peggi' ),
					'qodef-grid-1200' => esc_html__( '1200px', 'peggi' ),
					'qodef-grid-1000' => esc_html__( '1000px', 'peggi' ),
					'qodef-grid-800'  => esc_html__( '800px', 'peggi' )
				)
			)
		);
		
		peggi_select_add_admin_field(
			array(
				'name'          => 'preload_pattern_image',
				'type'          => 'image',
				'label'         => esc_html__( 'Preload Pattern Image', 'peggi' ),
				'description'   => esc_html__( 'Choose preload pattern image to be displayed until images are loaded', 'peggi' ),
				'parent'        => $panel_design_style
			)
		);
		
		/***************** Content Layout - end **********************/
		
		$panel_settings = peggi_select_add_admin_panel(
			array(
				'page'  => '',
				'name'  => 'panel_settings',
				'title' => esc_html__( 'Settings', 'peggi' )
			)
		);
		
		/***************** Smooth Scroll Layout - begin **********************/
		
		peggi_select_add_admin_field(
			array(
				'name'          => 'page_smooth_scroll',
				'type'          => 'yesno',
				'default_value' => 'no',
				'label'         => esc_html__( 'Smooth Scroll', 'peggi' ),
				'description'   => esc_html__( 'Enabling this option will perform a smooth scrolling effect on every page (except on Mac and touch devices)', 'peggi' ),
				'parent'        => $panel_settings
			)
		);
		
		/***************** Smooth Scroll Layout - end **********************/
		
		/***************** Smooth Page Transitions Layout - begin **********************/
		
		peggi_select_add_admin_field(
			array(
				'name'          => 'smooth_page_transitions',
				'type'          => 'yesno',
				'default_value' => 'no',
				'label'         => esc_html__( 'Smooth Page Transitions', 'peggi' ),
				'description'   => esc_html__( 'Enabling this option will perform a smooth transition between pages when clicking on links', 'peggi' ),
				'parent'        => $panel_settings
			)
		);
		
			$page_transitions_container = peggi_select_add_admin_container(
				array(
					'parent'          => $panel_settings,
					'name'            => 'page_transitions_container',
					'dependency' => array(
						'show' => array(
							'smooth_page_transitions'  => 'yes'
						)
					)
				)
			);
		
				peggi_select_add_admin_field(
					array(
						'name'          => 'page_transition_preloader',
						'type'          => 'yesno',
						'default_value' => 'no',
						'label'         => esc_html__( 'Enable Preloading Animation', 'peggi' ),
						'description'   => esc_html__( 'Enabling this option will display an animated preloader while the page content is loading', 'peggi' ),
						'parent'        => $page_transitions_container
					)
				);
				
				$page_transition_preloader_container = peggi_select_add_admin_container(
					array(
						'parent'          => $page_transitions_container,
						'name'            => 'page_transition_preloader_container',
						'dependency' => array(
							'show' => array(
								'page_transition_preloader'  => 'yes'
							)
						)
					)
				);
				
					peggi_select_add_admin_field(
						array(
							'name'   => 'smooth_pt_bgnd_color',
							'type'   => 'color',
							'label'  => esc_html__( 'Page Loader Background Color', 'peggi' ),
							'parent' => $page_transition_preloader_container
						)
					);
					
					$group_pt_spinner_animation = peggi_select_add_admin_group(
						array(
							'name'        => 'group_pt_spinner_animation',
							'title'       => esc_html__( 'Loader Style', 'peggi' ),
							'description' => esc_html__( 'Define styles for loader spinner animation', 'peggi' ),
							'parent'      => $page_transition_preloader_container
						)
					);
					
					$row_pt_spinner_animation = peggi_select_add_admin_row(
						array(
							'name'   => 'row_pt_spinner_animation',
							'parent' => $group_pt_spinner_animation
						)
					);
					
					peggi_select_add_admin_field(
						array(
							'type'          => 'selectsimple',
							'name'          => 'smooth_pt_spinner_type',
							'default_value' => '',
							'label'         => esc_html__( 'Spinner Type', 'peggi' ),
							'parent'        => $row_pt_spinner_animation,
							'options'       => array(
								'peggi_loader'          => esc_html__( 'Peggi Loader', 'peggi' ),
								'rotate_circles'        => esc_html__( 'Rotate Circles', 'peggi' ),
								'pulse'                 => esc_html__( 'Pulse', 'peggi' ),
								'double_pulse'          => esc_html__( 'Double Pulse', 'peggi' ),
								'cube'                  => esc_html__( 'Cube', 'peggi' ),
								'rotating_cubes'        => esc_html__( 'Rotating Cubes', 'peggi' ),
								'stripes'               => esc_html__( 'Stripes', 'peggi' ),
								'wave'                  => esc_html__( 'Wave', 'peggi' ),
								'two_rotating_circles'  => esc_html__( '2 Rotating Circles', 'peggi' ),
								'five_rotating_circles' => esc_html__( '5 Rotating Circles', 'peggi' ),
								'atom'                  => esc_html__( 'Atom', 'peggi' ),
								'clock'                 => esc_html__( 'Clock', 'peggi' ),
								'mitosis'               => esc_html__( 'Mitosis', 'peggi' ),
								'lines'                 => esc_html__( 'Lines', 'peggi' ),
								'fussion'               => esc_html__( 'Fussion', 'peggi' ),
								'wave_circles'          => esc_html__( 'Wave Circles', 'peggi' ),
								'pulse_circles'         => esc_html__( 'Pulse Circles', 'peggi' )
							)
						)
					);
					
					peggi_select_add_admin_field(
						array(
							'type'          => 'colorsimple',
							'name'          => 'smooth_pt_spinner_color',
							'default_value' => '',
							'label'         => esc_html__( 'Spinner Color', 'peggi' ),
							'parent'        => $row_pt_spinner_animation
						)
					);
					
					peggi_select_add_admin_field(
						array(
							'name'          => 'page_transition_fadeout',
							'type'          => 'yesno',
							'default_value' => 'no',
							'label'         => esc_html__( 'Enable Fade Out Animation', 'peggi' ),
							'description'   => esc_html__( 'Enabling this option will turn on fade out animation when leaving page', 'peggi' ),
							'parent'        => $page_transitions_container
						)
					);
		
		/***************** Smooth Page Transitions Layout - end **********************/
		
		peggi_select_add_admin_field(
			array(
				'name'          => 'show_back_button',
				'type'          => 'yesno',
				'default_value' => 'yes',
				'label'         => esc_html__( 'Show "Back To Top Button"', 'peggi' ),
				'description'   => esc_html__( 'Enabling this option will display a Back to Top button on every page', 'peggi' ),
				'parent'        => $panel_settings
			)
		);
		
		peggi_select_add_admin_field(
			array(
				'name'          => 'responsiveness',
				'type'          => 'yesno',
				'default_value' => 'yes',
				'label'         => esc_html__( 'Responsiveness', 'peggi' ),
				'description'   => esc_html__( 'Enabling this option will make all pages responsive', 'peggi' ),
				'parent'        => $panel_settings
			)
		);
		
		$panel_custom_code = peggi_select_add_admin_panel(
			array(
				'page'  => '',
				'name'  => 'panel_custom_code',
				'title' => esc_html__( 'Custom Code', 'peggi' )
			)
		);
		
		peggi_select_add_admin_field(
			array(
				'name'        => 'custom_js',
				'type'        => 'textarea',
				'label'       => esc_html__( 'Custom JS', 'peggi' ),
				'description' => esc_html__( 'Enter your custom Javascript here', 'peggi' ),
				'parent'      => $panel_custom_code
			)
		);
		
		$panel_google_api = peggi_select_add_admin_panel(
			array(
				'page'  => '',
				'name'  => 'panel_google_api',
				'title' => esc_html__( 'Google API', 'peggi' )
			)
		);
		
		peggi_select_add_admin_field(
			array(
				'name'        => 'google_maps_api_key',
				'type'        => 'text',
				'label'       => esc_html__( 'Google Maps Api Key', 'peggi' ),
				'description' => esc_html__( 'Insert your Google Maps API key here. For instructions on how to create a Google Maps API key, please refer to our to our documentation.', 'peggi' ),
				'parent'      => $panel_google_api
			)
		);
	}
	
	add_action( 'peggi_select_action_options_map', 'peggi_select_general_options_map', 1 );
}

if ( ! function_exists( 'peggi_select_page_general_style' ) ) {
	/**
	 * Function that prints page general inline styles
	 */
	function peggi_select_page_general_style( $style ) {
		$current_style = '';
		$page_id       = peggi_select_get_page_id();
		$class_prefix  = peggi_select_get_unique_page_class( $page_id );
		
		$boxed_background_style = array();
		
		$boxed_page_background_color = peggi_select_get_meta_field_intersect( 'page_background_color_in_box', $page_id );
		if ( ! empty( $boxed_page_background_color ) ) {
			$boxed_background_style['background-color'] = $boxed_page_background_color;
		}
		
		$boxed_page_background_image = peggi_select_get_meta_field_intersect( 'boxed_background_image', $page_id );
		if ( ! empty( $boxed_page_background_image ) ) {
			$boxed_background_style['background-image']    = 'url(' . esc_url( $boxed_page_background_image ) . ')';
			$boxed_background_style['background-position'] = 'center 0px';
			$boxed_background_style['background-repeat']   = 'no-repeat';
		}
		
		$boxed_page_background_pattern_image = peggi_select_get_meta_field_intersect( 'boxed_pattern_background_image', $page_id );
		if ( ! empty( $boxed_page_background_pattern_image ) ) {
			$boxed_background_style['background-image']    = 'url(' . esc_url( $boxed_page_background_pattern_image ) . ')';
			$boxed_background_style['background-position'] = '0px 0px';
			$boxed_background_style['background-repeat']   = 'repeat';
		}
		
		$boxed_page_background_attachment = peggi_select_get_meta_field_intersect( 'boxed_background_image_attachment', $page_id );
		if ( ! empty( $boxed_page_background_attachment ) ) {
			$boxed_background_style['background-attachment'] = $boxed_page_background_attachment;
		}
		
		$boxed_background_selector = $class_prefix . '.qodef-boxed .qodef-wrapper';
		
		if ( ! empty( $boxed_background_style ) ) {
			$current_style .= peggi_select_dynamic_css( $boxed_background_selector, $boxed_background_style );
		}
		
		$paspartu_style     = array();
		$paspartu_res_style = array();
		$paspartu_res_start = '@media only screen and (max-width: 1024px) {';
		$paspartu_res_end   = '}';
		
		$paspartu_header_selector                = array(
			'.qodef-paspartu-enabled .qodef-page-header .qodef-fixed-wrapper.fixed',
			'.qodef-paspartu-enabled .qodef-sticky-header',
			'.qodef-paspartu-enabled .qodef-mobile-header.mobile-header-appear .qodef-mobile-header-inner'
		);
		$paspartu_header_appear_selector         = array(
			'.qodef-paspartu-enabled.qodef-fixed-paspartu-enabled .qodef-page-header .qodef-fixed-wrapper.fixed',
			'.qodef-paspartu-enabled.qodef-fixed-paspartu-enabled .qodef-sticky-header.header-appear',
			'.qodef-paspartu-enabled.qodef-fixed-paspartu-enabled .qodef-mobile-header.mobile-header-appear .qodef-mobile-header-inner'
		);
		
		$paspartu_header_style                   = array();
		$paspartu_header_appear_style            = array();
		$paspartu_header_responsive_style        = array();
		$paspartu_header_appear_responsive_style = array();
		
		$paspartu_color = peggi_select_get_meta_field_intersect( 'paspartu_color', $page_id );
		if ( ! empty( $paspartu_color ) ) {
			$paspartu_style['background-color'] = $paspartu_color;
		}
		
		$paspartu_width = peggi_select_get_meta_field_intersect( 'paspartu_width', $page_id );
		if ( $paspartu_width !== '' ) {
			if ( peggi_select_string_ends_with( $paspartu_width, '%' ) || peggi_select_string_ends_with( $paspartu_width, 'px' ) ) {
				$paspartu_style['padding'] = $paspartu_width;
				
				$paspartu_clean_width      = peggi_select_string_ends_with( $paspartu_width, '%' ) ? peggi_select_filter_suffix( $paspartu_width, '%' ) : peggi_select_filter_suffix( $paspartu_width, 'px' );
				$paspartu_clean_width_mark = peggi_select_string_ends_with( $paspartu_width, '%' ) ? '%' : 'px';
				
				$paspartu_header_style['left']              = $paspartu_width;
				$paspartu_header_style['width']             = 'calc(100% - ' . ( 2 * $paspartu_clean_width ) . $paspartu_clean_width_mark . ')';
				$paspartu_header_appear_style['margin-top'] = $paspartu_width;
			} else {
				$paspartu_style['padding'] = $paspartu_width . 'px';
				
				$paspartu_header_style['left']              = $paspartu_width . 'px';
				$paspartu_header_style['width']             = 'calc(100% - ' . ( 2 * $paspartu_width ) . 'px)';
				$paspartu_header_appear_style['margin-top'] = $paspartu_width . 'px';
			}
		}
		
		$paspartu_selector = $class_prefix . '.qodef-paspartu-enabled .qodef-wrapper';
		
		if ( ! empty( $paspartu_style ) ) {
			$current_style .= peggi_select_dynamic_css( $paspartu_selector, $paspartu_style );
		}
		
		if ( ! empty( $paspartu_header_style ) ) {
			$current_style .= peggi_select_dynamic_css( $paspartu_header_selector, $paspartu_header_style );
			$current_style .= peggi_select_dynamic_css( $paspartu_header_appear_selector, $paspartu_header_appear_style );
		}
		
		$paspartu_responsive_width = peggi_select_get_meta_field_intersect( 'paspartu_responsive_width', $page_id );
		if ( $paspartu_responsive_width !== '' ) {
			if ( peggi_select_string_ends_with( $paspartu_responsive_width, '%' ) || peggi_select_string_ends_with( $paspartu_responsive_width, 'px' ) ) {
				$paspartu_res_style['padding'] = $paspartu_responsive_width;
				
				$paspartu_clean_width      = peggi_select_string_ends_with( $paspartu_responsive_width, '%' ) ? peggi_select_filter_suffix( $paspartu_responsive_width, '%' ) : peggi_select_filter_suffix( $paspartu_responsive_width, 'px' );
				$paspartu_clean_width_mark = peggi_select_string_ends_with( $paspartu_responsive_width, '%' ) ? '%' : 'px';
				
				$paspartu_header_responsive_style['left']              = $paspartu_responsive_width;
				$paspartu_header_responsive_style['width']             = 'calc(100% - ' . ( 2 * $paspartu_clean_width ) . $paspartu_clean_width_mark . ')';
				$paspartu_header_appear_responsive_style['margin-top'] = $paspartu_responsive_width;
			} else {
				$paspartu_res_style['padding'] = $paspartu_responsive_width . 'px';
				
				$paspartu_header_responsive_style['left']              = $paspartu_responsive_width . 'px';
				$paspartu_header_responsive_style['width']             = 'calc(100% - ' . ( 2 * $paspartu_responsive_width ) . 'px)';
				$paspartu_header_appear_responsive_style['margin-top'] = $paspartu_responsive_width . 'px';
			}
		}
		
		if ( ! empty( $paspartu_res_style ) ) {
			$current_style .= $paspartu_res_start . peggi_select_dynamic_css( $paspartu_selector, $paspartu_res_style ) . $paspartu_res_end;
		}
		
		if ( ! empty( $paspartu_header_responsive_style ) ) {
			$current_style .= $paspartu_res_start . peggi_select_dynamic_css( $paspartu_header_selector, $paspartu_header_responsive_style ) . $paspartu_res_end;
			$current_style .= $paspartu_res_start . peggi_select_dynamic_css( $paspartu_header_appear_selector, $paspartu_header_appear_responsive_style ) . $paspartu_res_end;
		}
		
		$current_style = $current_style . $style;
		
		return $current_style;
	}
	
	add_filter( 'peggi_select_filter_add_page_custom_style', 'peggi_select_page_general_style' );
}