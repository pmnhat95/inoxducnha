<?php

if ( ! function_exists( 'peggi_select_reset_options_map' ) ) {
	/**
	 * Reset options panel
	 */
	function peggi_select_reset_options_map() {
		
		peggi_select_add_admin_page(
			array(
				'slug'  => '_reset_page',
				'title' => esc_html__( 'Reset', 'peggi' ),
				'icon'  => 'fa fa-retweet'
			)
		);
		
		$panel_reset = peggi_select_add_admin_panel(
			array(
				'page'  => '_reset_page',
				'name'  => 'panel_reset',
				'title' => esc_html__( 'Reset', 'peggi' )
			)
		);
		
		peggi_select_add_admin_field(
			array(
				'type'          => 'yesno',
				'name'          => 'reset_to_defaults',
				'default_value' => 'no',
				'label'         => esc_html__( 'Reset to Defaults', 'peggi' ),
				'description'   => esc_html__( 'This option will reset all Select Options values to defaults', 'peggi' ),
				'parent'        => $panel_reset
			)
		);
	}
	
	add_action( 'peggi_select_action_options_map', 'peggi_select_reset_options_map', 100 );
}