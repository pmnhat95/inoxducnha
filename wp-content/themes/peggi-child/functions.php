<?php

/*** Child Theme Function  ***/
add_action( 'wp_enqueue_scripts', 'peggi_select_child_theme_enqueue_scripts', 11 );

function peggi_select_child_theme_enqueue_scripts() {
	wp_enqueue_style( 'peggi-select-child-style', get_stylesheet_uri() );
}

add_filter('woocommerce_order_button_text','custom_order_button_text',1);
function custom_order_button_text($order_button_text) {

	$order_button_text = 'Thanh toán';

	return $order_button_text;
}

function cart_custom_redirect_continue_shopping() {
	return get_home_url( null, '', null );
}
add_filter( 'woocommerce_continue_shopping_redirect', 'cart_custom_redirect_continue_shopping' );

function wc_empty_cart_redirect_url() {
	return get_home_url( null, '', null );
}
add_filter( 'woocommerce_return_to_shop_redirect', 'wc_empty_cart_redirect_url' );


add_filter( 'woocommerce_get_price_html', 'bbloomer_price_free_zero_empty', 100, 2 );

function bbloomer_price_free_zero_empty( $price, $product ){

	if ( '' === $product->get_price() || 0 == $product->get_price() ) {
    	$price = '<span class="woocommerce-Price-amount amount">Liên hệ</span>';
	}

	return $price;
}

