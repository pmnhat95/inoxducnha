<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'db1');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'mysql');

/** MySQL hostname */
define('DB_HOST', '172.17.0.3');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '=Mxw]Mh;Sp 01_tv*m/2@t}USUSQ+?*P-KPntUbA]^j_1Ju8zr7-_xUmvs$KMd3P');
define('SECURE_AUTH_KEY',  'cz+O,B:C!GG3l-Q:->,8hidkxb<{%7%o~-HTq.`CCeA<c9I{&gO<Op5oXa fmzvu');
define('LOGGED_IN_KEY',    '-#evb01!mv?H[E*+m.X-0yY3ARu*RL!`|)<=UxWz<lhF;[M-^iS?+ZlTB+w$@rSU');
define('NONCE_KEY',        'R1|wOLk;ry72zo~X6+Cfb?q8A_UVPe%mjJ9Pkzoy}^qfeFcx/wBWFZ[L4TI-%a-l');
define('AUTH_SALT',        'gV^[|+l3{o&=hxmrzs?;_eHX0yC|_|-YY>QSx{vX&^FB~9x3CO!H.1wR:1mJ^YF ');
define('SECURE_AUTH_SALT', 'ZuZ:@$Dd1`$HFI6AG:TjrXG/lbwe,A)#k-$&_i_HZB7H+,}uqys|/)l|D.i~>}e1');
define('LOGGED_IN_SALT',   '/EXya26fI_Kt%SAy5Syf$@@(wok?qmyjA83pM<H:;6O*!]!B^G;X>`{-(n+;-UB[');
define('NONCE_SALT',       'oW_|l&YR(AL$h%wQ/;hbJ3+,9TR`Xv4]l--iQ-Oe{ xzcs} <,MuAreoZBqPH9G]');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

//Multi Domain for a site
define('WP_SITEURL', 'http://' . $_SERVER['HTTP_HOST']);
define('WP_HOME', 'http://' . $_SERVER['HTTP_HOST']);


define('DISALLOW_FILE_EDIT',true);
define('DISALLOW_FILE_MODS',true);
define('FS_METHOD','direct');